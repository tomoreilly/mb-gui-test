#version 330
// layout(location = 0) in vec3 position;
// layout(location = 1) in vec3 normal;
// layout(location = 2) in vec4 color;
attribute vec3 a_Position;
attribute vec3 a_Normal;
attribute vec4 a_Color;

// Transformation matrix
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

out vec3 vPosition;
out vec3 vNormal;
out vec4 vColor;
out vec3 vFragWorldPos;

void main()
{
  // Apply transformation matrix to position
  gl_Position = projection * view * model * vec4(a_Position, 1.0);

  // Output
  vPosition = vec3(gl_Position);
  vNormal = vec3(a_Normal);
  vColor = a_Color;
  vFragWorldPos = vec3(model * vec4(a_Position, 1.0));
}
