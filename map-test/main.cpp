#include <QGuiApplication>
#include "MapWindow.h"

int main(int argc, char *argv[])
{
  if (argc < 2) {
      fprintf(stderr, "usage: %s gridfile\n", argv[0]);
      return -1;
    }

  char *gridfile = argv[1];

  QGuiApplication app(argc, argv);

  // Set OpenGL Version information
  // Note: This format must be set before show() is called.
  QSurfaceFormat format;
  format.setRenderableType(QSurfaceFormat::OpenGL);
  format.setProfile(QSurfaceFormat::CoreProfile);
  format.setVersion(3, 3);


  // Set up the window
  MapWindow pwindow;
  if (!pwindow.loadTopoGrid(gridfile)) {
      qCritical() << "Couldn't load grid file " << gridfile;
    }
  pwindow.setFormat(format);
  pwindow.resize(QSize(800, 600));
  pwindow.show();

  return app.exec();
}
