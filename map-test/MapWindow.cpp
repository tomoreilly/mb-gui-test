#include "MapWindow.h"
#include <QDebug>
#include <QString>
#include <QOpenGLShaderProgram>
#include <QVector3D>
#include <QResizeEvent>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <float.h>

const float MapWindow::MinPosition = -5.;     // original -5.
const float MapWindow::PositionRange = 10.;   // original 10.

const char * MapWindow::PositionAttrName = "a_Position";
const char * MapWindow::NormalAttrName = "a_Normal";
const char * MapWindow::ColorAttrName = "a_Color";
const char * MapWindow::LightPositionName = "u_LightPos";
const char * MapWindow::MvpMatrixName = "u_MVPMatrix";
const char * MapWindow::MvMatrixName = "u_MVMatrix";

/* ***
float testVertices[] = {
  0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f, // top right
  0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom right
  -0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 0.0f, 1.0f, 1.0f, // bottom left
  -0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f   // top left
};
*** */

float testVertices[] = {
  501.5f,  501.5f, -1000.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f, // top right
  501.5f, 500.5f, -1000.0f, 0.f, 0.f, 0.f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom right
  500.5f, 500.5f, -1000.0f, 0.f, 0.f, 0.f, 0.0f, 0.0f, 1.0f, 1.0f, // bottom left
  500.5f,  501.5f, -1000.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f   // top left
};

GLuint testIndices[] = {  // note that we start from 0!
                          0, 1, 3,   // first triangle
                          1, 2, 3    // second triangle
                       };

MapWindow::MapWindow() :
  m_gridVertices(nullptr), m_gridIndices(nullptr),
  m_nGridVertices(0), m_nGridIndices(0),
  m_ibo(QOpenGLBuffer::IndexBuffer), m_testData(false),
  m_modelLightPos(0.f, 0.f, 0.f, 1.0f)
{

}


void MapWindow::initializeGL()
{
  // Initialize OpenGL Backend
  initializeOpenGLFunctions();
  m_openGLInitialized = true;


  if (!m_gmtGrid) {
      qCritical() << "Topography has no GMT Grid data yet";
      return;
    }

  setData(m_gmtApi, m_gmtGrid);

  QSize wSize = size();
  resizeGL(wSize.width(), wSize.height());

  connect(context(), SIGNAL(aboutToBeDestroyed()), this,
          SLOT(teardownGL()), Qt::DirectConnection);

  // Initialze matrices
  initializeMatrices();

  // Enable depth-testing
  glEnable(GL_DEPTH_TEST);

  glClearColor(0.f, 0.f, 0.f, 1.0f);

  // Application-specific stuff
  {
    const char *vertexShader = ":/shaders/test.vert";
    const char *fragmentShader = ":/shaders/test.frag";

    // Create shader program, add, link and bind shaders
    m_program = new QOpenGLShaderProgram();
    fprintf(stderr, "m_program: %p\n", m_program);
    if (!m_program->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                            vertexShader)) {
        // Error
        QString msg = m_program->log();
        qCritical("Error adding vertex shader:\n" + msg.toLatin1());
        return;
      }
    qDebug("added vertex shader");

    if (!m_program->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                            fragmentShader)) {
        // Error
        QString msg = m_program->log();
        qCritical("Error adding fragment shader:\n" + msg.toLatin1());
        return;
      }
    qDebug("added fragment shader");

    if (!m_program->link()) {
        // Error
        fprintf(stderr, "shader linking failed\n");
        return;
      }
    qDebug("shaders linked ok");

    m_program->bind();

    if (!m_vao.create()) {
        // Error
        fprintf(stderr, "VAO create() failed\n");
        return;
      }
    m_vao.bind();

    m_vbo.create();
    m_vbo.bind();
    m_vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_ibo.create();
    m_ibo.bind();
    m_ibo.setUsagePattern(QOpenGLBuffer::StaticDraw);

    if (m_testData) {
        m_vbo.allocate(testVertices, sizeof(testVertices));
        m_ibo.allocate(testIndices, sizeof(testIndices));
      }
    else {
        m_vbo.allocate(m_gridVertices, m_nGridVertices * m_floatsPerVertex * sizeof(GL_FLOAT));
        m_ibo.allocate(m_gridIndices, m_nGridIndices * sizeof(GL_UNSIGNED_INT));
      }

    // Configure attributes for shader
    m_positionAttr = glGetAttribLocation(m_program->programId(), PositionAttrName);
    m_normalAttr = glGetAttribLocation(m_program->programId(), NormalAttrName);
    m_colorAttr = glGetAttribLocation(m_program->programId(), ColorAttrName);
    fprintf(stderr, "positionAttr: %d, normalAttr: %d, colorAttr: %d\n",
            m_positionAttr, m_normalAttr, m_colorAttr);

    m_program->setAttributeBuffer(m_positionAttr, GL_FLOAT, 0,
                                  m_nPositionTuples, Stride);

    m_program->setAttributeBuffer(m_normalAttr, GL_FLOAT,
                                  m_nPositionTuples * sizeof(GL_FLOAT),
                                  m_nNormalTuples, Stride);

    m_program->setAttributeBuffer(m_colorAttr, GL_FLOAT,
                                  (m_nPositionTuples + m_nNormalTuples) *
                                  sizeof(GL_FLOAT),
                                  m_nColorTuples, Stride);

    m_program->enableAttributeArray(m_positionAttr);
    m_program->enableAttributeArray(m_normalAttr);
    m_program->enableAttributeArray(m_colorAttr);

    // Unbind
    // do NOT release the IBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the IBO bound.
    // (https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/2.2.hello_triangle_indexed/hello_triangle_indexed.cpp)
    // m_ibo.release();
    m_vbo.release();
    m_vao.release();
    m_program->release();
  }
}

void MapWindow::initializeMatrices() {

  qDebug("initializeMatrices()");
  return;
}

void MapWindow::resizeGL(int width, int height)
{
  qDebug("MapWindow::resizeGL()");
  if (!m_openGLInitialized) {
      qDebug("resizeGL() - OpenGL not yet initialized");
      return;
    }
  qDebug("MapWindow::resizeGL - set viewport size");
  // Set OpenGL viewport to be the same dimension as window
  glViewport(0, 0, width, height);

  // Create a new perspective projection matrix. The height will stay the
  // same while the width will vary as per aspect ratio.
  float ratio = (float) width / height;
  float left = -ratio;
  float right = ratio;
  float bottom = -1.0f;
  float top = 1.0f;
  float near = 1.0f;
  float far = 1000.0f;

  m_projectionMatrix.frustum(left, right, bottom, top, near, far);
}

void MapWindow::setData(void *gmtApi, GMT_GRID *gmtGrid) {

  struct GMT_GRID_HEADER *header = gmtGrid->header;

  m_nGridVertices = header->n_columns * header->n_rows;

  // Create vertices for grid data
  m_gridVertices = (GLfloat *)malloc(m_nGridVertices * m_floatsPerVertex * sizeof(GLfloat));

  // Get zRange and zMin for coloring algorithm
  float zRange = (header->z_max - header->z_min);
  float zMin = header->z_min;

  float zPrev = 0.f;
  int vIndex = 0;   // Index into m_gridVertices array
  // Populate vertices with GMT_GRID data
  for (uint row = 0; row < header->n_rows; row++) {
      for (uint col = 0; col < header->n_columns; col++) {
          uint dataInd = GMT_Get_Index(gmtApi, gmtGrid->header, row, col);
          float z = gmtGrid->data[dataInd];
          if (isnanf(z)) {
              z = zPrev;  // Extrapolate previous value
            }

          m_gridVertices[vIndex++] = (float )gmtGrid->x[col];
          m_gridVertices[vIndex++] = (float )gmtGrid->y[row];
          m_gridVertices[vIndex++] = z;
          zPrev = z;

          // Estimate normal vector at this point, based on slope
          if (col > 0 && row > 0) {
              // Get z at (prev-x, y)
              uint prevInd = GMT_Get_Index(gmtApi, gmtGrid->header, row, col-1);
              float slopeX = (z - gmtGrid->data[prevInd]) / (float )(gmtGrid->x[col] - gmtGrid->x[col-1]);

              // Get z at (x, prev-y)
              prevInd = (row - 1) * header->n_rows + col;
              prevInd = GMT_Get_Index(gmtApi, gmtGrid->header, row-1, col);
              float slopeY = (z - gmtGrid->data[prevInd]) / (float )(gmtGrid->y[row] - gmtGrid->y[row-1]);

              // Normal vector is cross-product of slopeX ad slopeY
              const float planeVecX[] = {1.f, 0., slopeX};
              const float planeVecY[] = {0., 1.f, slopeY};

              float normVec[] =
              {(planeVecX[1] * planeVecY[2] - planeVecX[2] * planeVecY[1]),
               (planeVecX[2] * planeVecY[0] - planeVecX[0] * planeVecY[2]),
               (planeVecX[0] * planeVecY[1] - planeVecX[1] * planeVecY[0])};

              // Normalize the normal vector
              float length = sqrt(normVec[0]*normVec[0] +
                   normVec[1]*normVec[1] + normVec[2]*normVec[2]);

               m_gridVertices[vIndex++] = normVec[0] / length;
               m_gridVertices[vIndex++] = normVec[1] / length;
               m_gridVertices[vIndex++] = normVec[2] / length;
            }
          else {  // Point along x=0 or y=0 edge
              // Assume some slope for points at edge of grid
              m_gridVertices[vIndex++] = 0.f;
              m_gridVertices[vIndex++] = 0.f;
              m_gridVertices[vIndex++] = 0.f;
            }

          /* ***
          // Set normals to 0,0,0 for now
          m_gridVertices[vIndex++] = 0.f;
          m_gridVertices[vIndex++] = 0.f;
          m_gridVertices[vIndex++] = 0.f;
          *** */

          float red = 1.f, green = 0.f, blue = 0.f;
          // Assign color based on z
          setColor(z, zMin, zRange, &red, &green, &blue);  // Return white for now!
          m_gridVertices[vIndex++] = red;  // red
          m_gridVertices[vIndex++] = green;  // green
          m_gridVertices[vIndex++] = blue;  // blue
          m_gridVertices[vIndex++] = 1.f;  // alpha
          // fprintf(stderr, "red: %.3f, green: %.1f, blue: %.1f :::", red, green, blue);
        }
    }

  qDebug() << "m_nGridVertices: " << m_nGridVertices << ", vIndex: " << vIndex;

  /* ***
  // DEBUG - print out vertices
  fprintf(stderr, "vertices:\n");
  for (int i = 0; i < m_nGridVertices * m_floatsPerVertex; i++) {
      if (!(i%10)) {
          fprintf(stderr, "\n");
        }
      fprintf(stderr, "%.4f ", m_gridVertices[i]);
    }
  fprintf(stderr, "\n");
  *** */

  // Index the triangles to be drawn
  // Specify two triangles (6 vertices) starting at each row, col
  m_nGridIndices = 6 * (header->n_rows) * (header->n_columns);
  qDebug() << "m_nGridIndices: " << m_nGridIndices;
  // Allocate triangle indices
  m_gridIndices = (GLuint *)malloc(m_nGridIndices * sizeof(GLuint));
  GLuint *indices = m_gridIndices;

  int gIndex = 0;
  for (int row = 0; row < header->n_rows - 1; row++) {
      for (int col = 0; col < header->n_columns - 1; col++) {

          // First triangle
          indices[gIndex++] = vertexIndex(col, row, header->n_columns);
          indices[gIndex++] = vertexIndex(col+1, row, header->n_columns);
          indices[gIndex++] = vertexIndex(col+1, row+1, header->n_columns);

          // Second triangle
          indices[gIndex++] = vertexIndex(col, row, header->n_columns);
          indices[gIndex++] = vertexIndex(col+1, row+1, header->n_columns);
          indices[gIndex++] = vertexIndex(col, row+1, header->n_columns);
          // qDebug() << "gIndex: " << gIndex;
        }
    }

  qDebug() << "m_nGridIdices: " << m_nGridIndices << ", gIndex: " << gIndex;
  m_nGridIndices = gIndex;  // Adjust the number of actual vertices in the index
  qDebug("Done with setData()");
}

GLuint MapWindow::vertexIndex(int col, int row, int nColumns) {
  return (GLuint)( row * nColumns + col );
}


void MapWindow::paintGL() {

  glClear(GL_COLOR_BUFFER_BIT);
  if (!m_program->bind()) {
      QString msg = m_program->log();
      qCritical("Couldn't bind program:\n" + msg.toLatin1());
      return;
    }

  GLint model = glGetUniformLocation(m_program->programId(), "model");
  if (model == -1) {
      qCritical() << "variable \"" << model << "\" not found in shader";
      return;
    }

  GLint view = glGetUniformLocation(m_program->programId(), "view");
  if (view == -1) {
      qCritical() << "variable \"" << view << "\" not found in shader";
      return;
    }

  GLint projection = glGetUniformLocation(m_program->programId(), "projection");
  if (projection == -1) {
      qCritical() << "variable \"" << projection << "\" not found in shader";
      return;
    }

  GLint lightPos = glGetUniformLocation(m_program->programId(), LightPositionName);
  if (lightPos == -1) {
      qCritical() << "variable " << LightPositionName << " not found in shader";
      return;
    }

  // Get map limits
  float xMin, xMax, yMin, yMax, zMin, zMax;
  getLimits(&xMin, &xMax, &yMin, &yMax, &zMin, &zMax);

  // Find center of grid
  float xCenter = (xMin + xMax) / 2.f;
  float yCenter = (yMin + yMax) / 2.f;

  float zScale = 1.f;
  if (zMax != zMin) {
      // Scale z values to x-axis data extent
      zScale = (xMax - xMin) / (zMax - zMin);
    }
  else if (zMax != 0.f){
      zScale = (xMax - xMin) / zMax;
    }

  qDebug("Set model to identity matrix");
  m_modelMatrix.setToIdentity();

  qDebug() << "model zScale: " << zScale;
  // zScale *= 0.1f;
  m_modelMatrix.scale(1.f, 1.f, zScale);

  m_modelMatrix.rotate(-45.f, 1.f, 0.f, 0.f);

  qDebug() << "model translate - x: " << -xCenter << ", y: " << -yCenter;
  m_modelMatrix.translate(-xCenter, -yCenter, 0.f);


  m_viewMatrix.setToIdentity();
  // Move back along z-axis
  float zViewRange = -5.f * (xMax - xMin);
  qDebug() << "view - move back along z-axis to " << zViewRange;
  m_viewMatrix.translate(-0.f, 0.f, zViewRange);

  m_projectionMatrix.setToIdentity();
  m_projectionMatrix.perspective(45.f, 0.8, 0.1, 1000000.f);

  qDebug("send modelMatrix to shader");
  glUniformMatrix4fv(model, 1, false, m_modelMatrix.constData());

  qDebug("send viewMatrix to shader");
  glUniformMatrix4fv(view, 1, false, m_viewMatrix.constData());

  qDebug("send projectionMatrix to shader");
  glUniformMatrix4fv(projection, 1, false, m_projectionMatrix.constData());

  // Set lighting
  m_lightModelMatrix.setToIdentity();
  m_lightModelMatrix.translate(0.f, 1.f, 5.0f);

  // m_modelLightPos(0.f, 0.f, 0.f, 1.0f);
  m_modelLightPos.setZ(1.9f);
  m_worldLightPos = m_lightModelMatrix * m_modelLightPos;
  m_eyeLightPos = m_viewMatrix * m_worldLightPos;

  qDebug("send eye-light-position to shader");
  glUniform3f(lightPos, m_worldLightPos[0], m_worldLightPos[1], m_worldLightPos[2]);

  m_vao.bind();
  fprintf(stderr, "draw elements\n");
  if (m_testData) {
      glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
  else {
      // glDrawElements(GL_TRIANGLE_STRIP, m_nGridIndices, GL_UNSIGNED_INT, 0);
      glDrawElements(GL_TRIANGLES, m_nGridIndices, GL_UNSIGNED_INT, 0);
      // glDrawElements(GL_POINTS, m_nGridIndices, GL_UNSIGNED_INT, 0);
    }

  m_vao.release();

  m_program->release();
  glFlush();
  return;

}


void MapWindow::teardownGL()
{
  m_vbo.destroy();
  m_vao.destroy();
  m_ibo.destroy();
  
  delete m_program;
  
  fprintf(stderr, "HeightMapRenderer dstr\n");
  if (m_gridVertices) {
      free(m_gridVertices);
      m_gridVertices = nullptr;
    }
  if (m_gridIndices) {
      free(m_gridIndices);
      m_gridIndices = nullptr;
    }
}


GMT_GRID *MapWindow::readGridFile(const char *gridFile) {

  // Check for file existence and readability
  struct stat fileStatus;

  if (stat(gridFile, &fileStatus) != 0
      || (fileStatus.st_mode & S_IFMT) == S_IFDIR
      || fileStatus.st_size <= 0) {
      qCritical() << "Can not read \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile: %s\n", gridFile);
  // Create GMT API
  m_gmtApi =
      GMT_Create_Session("Topography::loadGrid()", 2U, 0U, nullptr);

  if (!m_gmtApi) {
      qCritical() << "Could not get GMT API for \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile now: %s\n", gridFile);

  // Try to read header and grid
  for (int nTry = 0; !m_gmtGrid && nTry < 100; nTry++) {
      m_gmtGrid = (struct GMT_GRID *)GMT_Read_Data(m_gmtApi, GMT_IS_GRID, GMT_IS_FILE, GMT_IS_SURFACE,
                                                   GMT_GRID_ALL, nullptr, gridFile, nullptr);
      if (m_gmtGrid) break;
      usleep(1000);
    }

  if (!m_gmtGrid) {
      qCritical() << "Unable to read GMT grid from \"" << gridFile << "\"";
      return nullptr;
    }
  return m_gmtGrid;
}


bool MapWindow::loadTopoGrid(const char *gridFile) {

  m_gmtGrid = readGridFile(gridFile);

  if (!m_gmtGrid) {
      qCritical() << "Failed to load grid \"" << gridFile;
      return false;
    }

  return true;
}


void MapWindow::setColor(float z, float zmin, float zrange, float *red, float *green, float *blue) {
  float ratio = (z - zmin) / zrange;
  *red = 1. - ratio;
  *blue = ratio;
  *green = 0.f;

  // TEST
  /***
  *red = 1.f;
  *green = 1.f;
  *blue = 1.f;
  * ***/
}


/// Get map limits
void MapWindow::getLimits(float *xMin, float *xMax, float *yMin, float *yMax, float *zMin, float *zMax) {

  *xMin = FLT_MAX;
  *xMax = -FLT_MAX;
  *yMin = FLT_MAX;
  *yMax = -FLT_MAX;
  *zMin = FLT_MAX;
  *zMax = -FLT_MAX;

  if (m_testData) {

      for (int i = 0; i < sizeof(testVertices)/sizeof(float); i += m_floatsPerVertex) {
          if (testVertices[i] < *xMin) {
              *xMin = testVertices[i];
            }
          if (testVertices[i] > *xMax) {
              *xMax = testVertices[i];
            }
          if (testVertices[i+1] < *yMin) {
              *yMin = testVertices[i+1];
            }
          if (testVertices[i+1] > *yMax) {
              *yMax = testVertices[i+1];
            }
          if (testVertices[i+2] < *zMin) {
              *zMin = testVertices[i+2];
            }
          if (testVertices[i+2] > *zMax) {
              *zMax = testVertices[i+2];
            }
        }
    }
  else {
      *xMin = m_gmtGrid->header->wesn[0];
      *xMax = m_gmtGrid->header->wesn[1];
      *yMin = m_gmtGrid->header->wesn[2];
      *yMax = m_gmtGrid->header->wesn[3];
      *zMin = m_gmtGrid->header->z_min;
      *zMax = m_gmtGrid->header->z_max;
    }
}
