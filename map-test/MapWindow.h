#ifndef MAPWINDOW_H
#define MAPWINDOW_H

#include <QOpenGLWindow>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QMatrix4x4>
#include <gmt/gmt.h>

class QOpenGLShaderProgram;

class MapWindow : public QOpenGLWindow,
               protected QOpenGLFunctions
{
  Q_OBJECT

// OpenGL Events
public:
  MapWindow();
  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();

  /// Load grid from file, set member GMT_GRID pointer
  bool loadTopoGrid(const char *gridFile);

  protected slots:
  void teardownGL();

private slots:
  /// Called when window size changes
  // void resizeEvent(QResizeEvent *event);

 protected:

  /// Get map limits, either from testVertices or from GMT grid
  void getLimits(float *xMin, float *xMax, float *yMin, float *yMax, float *zMin, float *zMax);

  void setColor(float z, float zmin, float zrange, float *red, float *green, float *blue);

  /// Read grid from GMT file; return pointer to GMT_GRID if successful, else return nullptr
  GMT_GRID *readGridFile(const char *filename);

  /// Return offset into row-major data array at row and col with each row containing nColumns
  static GLuint vertexIndex(int col, int row, int nColumns);

  /// Indicates if OpenGL functions are initialized
  bool m_openGLInitialized = false;

  /// Set parabolic function vertices
  void setData(void *gmtApi, GMT_GRID *gmtGrid);

  /// Initialize matrices
  void initializeMatrices();

  /// GMT API pointer
  void *m_gmtApi = nullptr;

  /// GMT grid (read from GMT file)
  struct GMT_GRID *m_gmtGrid = nullptr;

  /// Shaders
  QOpenGLShaderProgram *m_program;

  /// VAO
  QOpenGLVertexArrayObject m_vao;

  /// VBO
  QOpenGLBuffer m_vbo;

  /// IBO
  QOpenGLBuffer m_ibo;

  /// Size of the viewport
  QSize m_viewportSize;

  /// Size characteristics of parabola height map
  static const int VerticesPerSide = 32;   // was 32
  /// x, y, z
  static const int m_nPositionTuples = 3;
  /// x, y, z
  static const int m_nNormalTuples = 3;
  /// R, G, B, Alpha
  static const int m_nColorTuples = 4;

  static const int m_floatsPerVertex = (m_nPositionTuples + m_nNormalTuples + m_nColorTuples);

  /// Value indicates no data
  // const float m_noDataValue = -1200.f;

  /// Model matrix; moves model from object space to world space
  QMatrix4x4 m_modelMatrix;

  /// Projection matrix; projects scene onto viewport
  QMatrix4x4 m_projectionMatrix;

  /// View matrix; "camera", transforms world space to eye space
  QMatrix4x4 m_viewMatrix;

  /// Light model matrix
  QMatrix4x4 m_lightModelMatrix;

  /// Combined matrix
  QMatrix4x4 m_mvpMatrix;

  /// Current rotation matrix
  QMatrix4x4 m_rotationMatrix;

  /// Accumulated rotation matrix
  QMatrix4x4 m_accumRotationMatrix;

  /// Temporary matrix
  QMatrix4x4 m_tempMatrix;

  /// Current light position in world space (after transformation
  /// via model matrix)
  QVector4D m_worldLightPos;

  /// Light position in eye space (after transformation via model-view
  /// matrix)
  QVector4D m_eyeLightPos;

  /// Light centered at model space origin
  QVector4D m_modelLightPos;

  /// Attributes for shaders
  static const char *PositionAttrName;
  int m_positionAttr = 0;
  static const char *NormalAttrName;
  int m_normalAttr = 0;
  static const char *ColorAttrName;
  int m_colorAttr = 0;

  /// Shader matrix "uniform" variables
  static const char *LightPositionName;
  static const char *MvpMatrixName;
  static const char *MvMatrixName;

  static const int Stride =
      sizeof(float) * (m_nPositionTuples + m_nNormalTuples + m_nColorTuples);

  static const float MinPosition;
  static const float PositionRange;

  GLfloat *m_gridVertices;
  GLuint *m_gridIndices;

  /// Number of elements in m_mapVertices array
  int m_nGridVertices;

  /// Number of elements in m_mapIndexData array
  int m_nGridIndices;

  bool m_testData;


};

#endif // PARABOLAWINDOW_H
