#include <QtDebug>
#include <QtDataVisualization/QSurface3DSeries>
#include "TopographyBackend.h"

using namespace QtDataVisualization;

TopographyBackend *TopographyBackend::m_lastInstance = nullptr;

/** Backend singleton provides topographic data to QML */
TopographyBackend::TopographyBackend(QObject *parent) : QObject(parent)
{
}

void TopographyBackend::set3DSeriesName(QString seriesNameString) {
  m_seriesQmlName = seriesNameString;
  qDebug() << "set3DSeriesName() - name: " << seriesNameString;
}

bool TopographyBackend::set3DSeriesData(QString seriesNameString) {

  char *seriesName = seriesNameString.toLatin1().data();

  if (g_appEngine->rootObjects().isEmpty()) {
      qDebug("rootObjects are empty");
    }
  QObject *object = g_rootWindow->findChild<QObject *>("topographySeries");
  if (!object) {
       qCritical() << "Couldn't find " << seriesNameString << " object";
    }
  // Find 3D data series object
  QSurface3DSeries *surface3DSeries =
      g_rootWindow->findChild<QSurface3DSeries *>("topographySeries");
  if (!surface3DSeries) {
      qCritical() << "Couldn't find QSurface3DSeries " << seriesNameString << " object";
      return false;
    }

  //// TEST TEST TEST - where to get topo file name?
  // char *dataFile = "Extravert.pam";
  bool simulated = true;
  TopoGrid *grid = new TopoGrid(simulated);

  float lonWidth = grid->m_wesn[1] - grid->m_wesn[0];
  float latHeight = grid->m_wesn[3] - grid->m_wesn[2];

  // Get array size
  int width = grid->nColumns();
  int height = grid->nRows();

  float stepX = lonWidth / float(width);
  float stepZ = latHeight / float(height);

  QSurfaceDataArray *data = new QSurfaceDataArray();
  data->reserve(height);

  // Read topo data into array
  for (int row = 0; row < height; row++) {
      float z = (latHeight - float(row) * stepZ) + grid->m_wesn[2];
      QSurfaceDataRow *newRow = new QSurfaceDataRow(width);
      for (int col = 0; col < width; col++) {
          float y;
          grid->getData(row, col, &y);
          float x = float(col) * stepX + grid->m_wesn[0];
          (*newRow)[col].setPosition(QVector3D(x, y, z));
        }
      *data << newRow;
    }

  // Create data proxy
  QSurfaceDataProxy *proxy = new QSurfaceDataProxy();
  proxy->resetArray(data);

  // Set data proxy for QML Surface3DSeries
  surface3DSeries->setDataProxy(proxy);
  return true;
}
