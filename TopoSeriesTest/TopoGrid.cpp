#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "TopoGrid.h"

#define MAGIC_NUMBER "P7"
#define WIDTH "WIDTH"
#define HEIGHT "HEIGHT"
#define DEPTH "DEPTH"
#define MAXVAL "MAXVAL"
#define ENDHDR "ENDHDR"
#define WESN "# wesn"

static float simData[9]  = {
  -100., -100., -100.,
  -200., -200., -200.,
  -300., -300., -300.
};

TopoGrid::TopoGrid(char *gridFile)
{
  if (!readDataFile(gridFile)) {
      fprintf(stderr, "Couldn't read grid from \"%s\"\n", gridFile);
    }
}

TopoGrid::TopoGrid(bool simulated) {
  if (!simulated) {
      return;
    }

    m_data = simData;
    m_nRows = 3;
    m_nColumns = 3;
    m_wesn[0] = -100.;
    m_wesn[1] - -99.;
    m_wesn[2] = 36.;
    m_wesn[3] = 37.;
}

bool TopoGrid::readDataFile(char *dataFileName) {
  FILE *dataFile;
  if ((dataFile = fopen(dataFileName, "r")) == nullptr) {
      fprintf(stderr, "Couldn't open grid file \"%s\"\n", dataFileName);
      return false;
    }

  // Assume ascii pam ("portable any map") file (http://netpbm.sourceforge.net/doc/pam.html)
  // Assume header elements all on separate lines
  // Assume a header comment with format "# wesn: westLon eastLon southLat northLat
  char headerLine[80];
  // First line must be magic number
  if (!fgets(headerLine, sizeof(headerLine), dataFile)) {
      fprintf(stderr, "Couldn't read first line of file\n");
      fclose(dataFile);
      return false;
    }
  if (strncmp(headerLine, MAGIC_NUMBER, strlen(MAGIC_NUMBER))) {
      fprintf(stderr, "Bad magic number on first line, expecting %s\n",
              headerLine, MAGIC_NUMBER);
      fclose(dataFile);
      return false;
    }
  bool gotGridCorners = false;
  bool error = false;
  while (true) {
      if (!fgets(headerLine, sizeof(headerLine), dataFile)) {
          fprintf(stderr, "EOF while reading header?\n");
          error = true;
          break;
        }
      if (!strncmp(headerLine, ENDHDR, strlen(ENDHDR))) {
          // End of header; data follows
          break;
        }
      else if (!strncmp(headerLine, WIDTH, strlen(WIDTH))) {
          m_nColumns = atoi(headerLine + strlen(WIDTH));
        }
      else if (!strncmp(headerLine, HEIGHT, strlen(HEIGHT))) {
          m_nRows = atoi(headerLine + strlen(HEIGHT));
        }
      else if (!strncmp(headerLine, WESN, strlen(WESN))) {
          // Comment specifies west east south north grid corners
          if (sscanf(headerLine + strlen(WESN), "%f %f %f %f",
                 &m_wesn[0], &m_wesn[1], &m_wesn[2], &m_wesn[3]) != 4) {
              fprintf(stderr, "Could't parse \"%s\"\n", headerLine);
              error = true;
              break;
            }
          gotGridCorners = true;
        }
      else {
          fprintf(stderr, "Header line: %s\n", headerLine);

        }
    }
  if (!gotGridCorners) {
      fprintf(stderr, "Comment \"# wesn: west east south north\" not found\n");
      error = true;
    }

  if (error) {
      fclose(dataFile);
      return false;
    }

  // Assume 8 characters per ascii elevation value, including whitespace
  int maxDataLineBytes = m_nColumns * 8;
  char *dataLine = (char *)malloc(maxDataLineBytes);
  int nDataValues = m_nRows * m_nColumns;
  m_data = (float *)malloc(nDataValues * sizeof(float));

  // Read and parse each row of data
  int rows = 0;
  int nValuesRead = 0;
  while (fgets(dataLine, maxDataLineBytes, dataFile)) {
      rows++;
      if (nValuesRead >= nDataValues) {
          error = true;
          break;
        }
      // Parse each value from row
    }
  fclose(dataFile);
  free(dataLine);

  if (nValuesRead != nDataValues) {
      error = true;
      fprintf(stderr, "Read %d elevation values, but expecting %d\n",
              nValuesRead, nDataValues);
    }
  if (rows != m_nRows) {
      error = true;
      fprintf(stderr, "Read %d rows, but expecting %d\n", rows, m_nRows);
    }


  if (error) {
      return false;
    }
  return true;
}

bool TopoGrid::getData(unsigned int row, unsigned int column, float *data) {
  if (row >= m_nRows || column >= m_nColumns) {
      return false;
    }

  int ind = row * m_nColumns + column;
  *data = m_data[ind];
  return true;
}
