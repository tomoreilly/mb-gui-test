#include <QtCore/QDebug>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickWindow>
#include <QtDataVisualization/QSurface3DSeries>
#include "TopographicSeries.h"
#include "TopographyBackend.h"

using namespace QtDataVisualization;

QQmlApplicationEngine *g_appEngine;
QQuickWindow *g_rootWindow;

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  // qmlRegisterExtendedType<QSurface3DSeries, TopographicSeries>("Test", 1, 0, "Surface3DSeries");
  qmlRegisterSingletonType<TopographyBackend>("backend", 1, 0, "BackEnd",
                                              TopographyBackend::qmlInstance);
  // QQmlApplicationEngine engine;
  g_appEngine = new QQmlApplicationEngine();

  const QUrl url(QStringLiteral("qrc:/main.qml"));


  QObject::connect(g_appEngine, &QQmlApplicationEngine::objectCreated,
                   &app, [url](QObject *obj, const QUrl &objUrl) {
    if (!obj && url == objUrl)
      QCoreApplication::exit(-1);
  }, Qt::QueuedConnection);

  g_appEngine->load(url);
  if (g_appEngine->rootObjects().isEmpty()) {
      qDebug("Empty rootObjects");
      return -1;
    }

  // Get root window pointer
  g_rootWindow = qobject_cast<QQuickWindow*>(g_appEngine->rootObjects().value(0));

  QObject *object = g_rootWindow->findChild<QObject *>("topographySeries");
  if (!object) {
      qDebug("Couldn't find topographySeries object");
    }
  else {
      qDebug("Found topographySeries object");
    }

  TopographyBackend *backend = TopographyBackend::lastInstance();
  backend->set3DSeriesData(backend->seriesQmlName());
  return app.exec();
}
