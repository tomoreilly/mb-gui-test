#include <QtCore/QDebug>
#include "TopographicSeries.h"
#include "TopoGrid.h"

bool simulated = true;

TopographicSeries::TopographicSeries(QObject *object) :
  m_topoGrid(simulated)
{
  qDebug("TopographicSeries constructor");
  m_surface3dSeries = static_cast<QSurface3DSeries *>(object);
}

QString TopographicSeries::getTopoFile() const {

}

void TopographicSeries::setTopoFile(QString topoFile) {
  qDebug() << "setTopoFile(): " << topoFile.toLatin1();
  m_topoFile = strdup(topoFile.toUtf8().data());
}

bool TopographicSeries::readTopography() {


  char *dataFile = (char *)"junk";
  // Open topo file
  TopoGrid *grid = new TopoGrid(dataFile);

  float lonWidth = grid->m_wesn[1] - grid->m_wesn[0];
  float latHeight = grid->m_wesn[3] - grid->m_wesn[2];

  // Get array size
  int width = grid->nColumns();
  int height = grid->nRows();

  float stepX = lonWidth / float(width);
  float stepZ = latHeight / float(height);

  QSurfaceDataArray *data = new QSurfaceDataArray();
  data->reserve(height);

  // Read topo data into array
  for (int row = 0; row < height; row++) {
      float z = (latHeight - float(row) * stepZ) + grid->m_wesn[2];
      QSurfaceDataRow *newRow = new QSurfaceDataRow(width);
      for (int col = 0; col < width; col++) {
          float y;
          grid->getData(row, col, &y);
          float x = float(col) * stepX + grid->m_wesn[0];
          (*newRow)[col].setPosition(QVector3D(x, y, z));
        }
      *data << newRow;
    }

  // Create data proxy
  QSurfaceDataProxy *proxy = new QSurfaceDataProxy();
  proxy->resetArray(data);

  // Set data proxy for QML Surface3DSeries
  m_surface3dSeries->setDataProxy(proxy);

}
