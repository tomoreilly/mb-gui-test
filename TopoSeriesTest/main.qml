import QtQuick 2.0
import QtQuick.Window 2.12
import QtDataVisualization 1.2
import backend 1.0

Window {

    visible: true
    id: mainWindow
    width: 640
    height: 480
    title: qsTr("TopoSeries test")

    Item {
        width: 640
        height: 480

        Surface3D {
            width: parent.width
            height: parent.height
            Surface3DSeries {
                id: topographySeries;
                objectName: "topographySeries"
                ItemModelSurfaceDataProxy {
                    itemModel: dataModel
                    // Mapping model roles to surface series rows, columns, and values.
                    rowRole: "longitude"
                    columnRole: "latitude"
                    yPosRole: "elevation"
                }
                Component.onCompleted: {
                    BackEnd.set3DSeriesName(objectName);
                    console.log("completed Surface3D creation")
                }
            }

        }
        ListModel {
            id: dataModel
            ListElement{ longitude: "20"; latitude: "10"; elevation: "1000"; }
            ListElement{ longitude: "21"; latitude: "10"; elevation: "1200"; }
            ListElement{ longitude: "22"; latitude: "10"; elevation: "900"; }
            ListElement{ longitude: "23"; latitude: "10"; elevation: "1100"; }
            ListElement{ longitude: "20"; latitude: "11"; elevation: "1000"; }
            ListElement{ longitude: "21"; latitude: "11"; elevation: "1300"; }
            ListElement{ longitude: "22"; latitude: "11"; elevation: "1250"; }
            ListElement{ longitude: "23"; latitude: "11"; elevation: "1200"; }
            ListElement{ longitude: "20"; latitude: "12"; elevation: "1100"; }
            ListElement{ longitude: "21"; latitude: "12"; elevation: "1000"; }
            ListElement{ longitude: "22"; latitude: "12"; elevation: "950"; }
            ListElement{ longitude: "23"; latitude: "12"; elevation: "900"; }
            ListElement{ longitude: "20"; latitude: "13"; elevation: "850"; }
            ListElement{ longitude: "21"; latitude: "13"; elevation: "800"; }
            ListElement{ longitude: "22"; latitude: "13"; elevation: "750"; }
            ListElement{ longitude: "23"; latitude: "13"; elevation: "700"; }
        }
    }
}

