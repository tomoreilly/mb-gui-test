#ifndef TOPOGRAPHICSERIES_H
#define TOPOGRAPHICSERIES_H

#include <QObject>
#include <QtDataVisualization/QSurface3DSeries>
#include "TopoGrid.h"

using namespace QtDataVisualization;
/**
 * TopographicSeries "extends" the Surface3DSeries QML item using the mechanism described at
 * https://doc.qt.io/archives/qt-5.10/qtqml-referenceexamples-extended-example.html
 * */
class TopographicSeries : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString topoFile READ getTopoFile WRITE setTopoFile NOTIFY topoFileChanged)

public:
  /// Constructor takes pointer to QML Surface3DSeries object
  TopographicSeries(QObject *object);

  QString getTopoFile() const;
  void setTopoFile(QString topoFile);

signals:
  void topoFileChanged();


protected:
  /// Read topographic data from file, returns true on success
  bool readTopography();

  /// Reference to QML Surface3DSeries object
  QSurface3DSeries *m_surface3dSeries;

  /// Name of topographic data file
  char *m_topoFile;

  /// TopoGrid
  TopoGrid m_topoGrid;


};

#endif // TOPOGRAPHICSERIES_H
