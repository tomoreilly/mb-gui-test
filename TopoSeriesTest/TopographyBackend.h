#ifndef TOPOGRAPHYBACKEND_H
#define TOPOGRAPHYBACKEND_H

#include <QObject>
#include <QQuickWindow>
#include <QQmlApplicationEngine>
#include <QtDataVisualization/QSurface3DSeries>
#include "TopoGrid.h"

using namespace QtDataVisualization;

class TopographyBackend : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(TopographyBackend)

public:
  explicit TopographyBackend(QObject *parent = nullptr);

  /// Set QML object name for topography 3D series; this
  /// function typically called by QML script.
  Q_INVOKABLE void set3DSeriesName(QString name);

  /// Set data for specified 3D series; returns true on success
  Q_INVOKABLE bool set3DSeriesData(QString seriesName);

  /// Return most recent instance created
  static TopographyBackend *lastInstance() {
    return m_lastInstance;
  }

  /// Return name of qml topograpy Surface3DSeries item
  QString seriesQmlName() {
    return m_seriesQmlName;
  }

  static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine) {
       Q_UNUSED(engine);
       Q_UNUSED(scriptEngine)

       m_lastInstance = new TopographyBackend();
       fprintf(stderr, "backend singleton ptr: %p\n", m_lastInstance);

       return m_lastInstance;
   }

signals:

public slots:

protected:
  QString m_seriesQmlName;
  QSurface3DSeries *m_series;

  static TopographyBackend *m_lastInstance;

};

// Application engine
extern QQmlApplicationEngine *g_appEngine;

// Root QML window
extern QQuickWindow *g_rootWindow;

#endif // TOPOGRAPHYBACKEND_H
