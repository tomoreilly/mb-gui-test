#ifndef TOPOGRID_H
#define TOPOGRID_H

/** Holds topographic data read from file */
class TopoGrid
{
public:
  TopoGrid(bool simulated);

  TopoGrid(char *dataFile);

  /// Read from file
  bool readDataFile(char *dataFile);

  /// Number of grid rows
  int nRows() {
    return m_nRows;
  }

  /// Number of grid columns
  int nColumns() {
    return m_nColumns;
  }

  /// Get data at specified row and column in grid, returns true on success
  bool getData(unsigned int row, unsigned int column, float *data);

  /// Grid corner coordinates; west east south north
  float m_wesn[4];

protected:
  int m_nRows;
  int m_nColumns;
  float *m_data;
};

#endif // TOPOGRID_H
