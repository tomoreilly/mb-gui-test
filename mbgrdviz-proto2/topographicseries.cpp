/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Data Visualization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <gmt/gmt.h>
#include <sys/stat.h>
#include <unistd.h>
#include "topographicseries.h"

using namespace QtDataVisualization;


/// Gradient scale colors
const QColor TopographicSeries::m_gradientColors[] = {
  Qt::black, Qt::darkBlue, Qt::green, Qt::yellow, Qt::red, Qt::darkRed
};

TopographicSeries::TopographicSeries(GMT_GRID *gmtGrid)
{
  m_gmtGrid = gmtGrid;

  setDrawMode(QSurface3DSeries::DrawSurface);
  setFlatShadingEnabled(true);
  toggleColorMap(true);
  // setBaseColor(Qt::white);
}

TopographicSeries::~TopographicSeries()
{
}


void TopographicSeries::setTopography(void *gmtApi, GMT_GRID *grid, float width, float height)
{
  int gridHeight = grid->header->n_rows;
  int gridWidth = grid->header->n_columns;

  QSurfaceDataArray *dataArray = new QSurfaceDataArray;
  dataArray->reserve(gridHeight);
  for (int row = 0; row < gridHeight; row++) {
      // z is north-south axis
      float z = grid->y[row];
      QSurfaceDataRow *newRow = new QSurfaceDataRow(gridWidth);
      for (int col = 0; col < gridWidth; col++) {

          // Get elevation data index at row, col
          int index = GMT_Get_Index(gmtApi, grid->header, row, col);
          // y is elevation at specified column and row
          float y = grid->data[index];   // (note that QSurface properly ignores NaN)

          // x is east-west axis
          float x = grid->x[col];

          (*newRow)[col].setPosition(QVector3D(x, y, z));
        }
      *dataArray << newRow;
    }

  dataProxy()->resetArray(dataArray);

  m_sampleCountX = float(gridWidth);
  m_sampleCountZ = float(gridHeight);
}

GMT_GRID *TopographicSeries::readGridFile(const char *gridFile, void **api) {

  // Check for file existence and readability
  struct stat fileStatus;

  if (stat(gridFile, &fileStatus) != 0
      || (fileStatus.st_mode & S_IFMT) == S_IFDIR
      || fileStatus.st_size <= 0) {
      qCritical() << "Can not read \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile: %s\n", gridFile);
  // Create GMT API
  *api =
      GMT_Create_Session("Topography::loadGrid()", 2U, 0U, nullptr);

  if (!*api) {
      qCritical() << "Could not get GMT API for \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile now: %s\n", gridFile);

  GMT_GRID *grid = nullptr;
  // Try to read header and grid
  for (int nTry = 0; nTry < 100; nTry++) {
      grid = (struct GMT_GRID *)GMT_Read_Data(*api, GMT_IS_GRID, GMT_IS_FILE, GMT_IS_SURFACE,
                                              GMT_GRID_ALL, nullptr, gridFile, nullptr);
      if (grid) break;
      usleep(1000);
    }

  if (!grid) {
      qCritical() << "Unable to read GMT grid from \"" << gridFile << "\"";
      return nullptr;
    }
  return grid;
}

void TopographicSeries::toggleColorMap(bool on) {

  if (on) {

      QLinearGradient grad;

      float ratio = 1.f;

      // int nGradLevels = sizeof(m_gradientColors) / sizeof(QColor);
      int nGradLevels = nGradientColors();
      qreal gradIncrement = 0.2;

      for (int i = 0; i < nGradLevels; i++) {
          qreal level = (qreal )(i * gradIncrement);
          grad.setColorAt(level, m_gradientColors[i] );
        }

      setBaseGradient(grad);
      setColorStyle(Q3DTheme::ColorStyleRangeGradient);
    }
  else {
      setBaseColor(Qt::white);
    }
}


int TopographicSeries::nGradientColors() {
  return sizeof(m_gradientColors) / sizeof(QColor);
}
