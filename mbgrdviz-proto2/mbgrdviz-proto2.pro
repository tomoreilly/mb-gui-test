android|ios|winrt {
    error( "This example is not supported for android, ios, or winrt." )
}

SOURCES += main.cpp \
           surfacegraph.cpp \
           topographicseries.cpp \
           highlightseries.cpp \
           custominputhandler.cpp

HEADERS += surfacegraph.h \
           topographicseries.h \
           highlightseries.h \
           custominputhandler.h

QT += datavisualization
QT += widgets

RESOURCES += mbgrdviz-proto2.qrc

OTHER_FILES += doc/src/* \
               doc/images/*

unix|win32: LIBS += -lgmt
