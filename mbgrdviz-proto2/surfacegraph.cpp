/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Data Visualization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <string.h>
#include "surfacegraph.h"
#include "topographicseries.h"

#include <QtDataVisualization/QValue3DAxis>
#include <QtDataVisualization/Q3DTheme>

using namespace QtDataVisualization;

// const float areaWidth = 8000.0f;
// const float areaHeight = 8000.0f;
// const float aspectRatio = 0.1389f;
// const float minRange = areaWidth * 0.49f;

SurfaceGraph::SurfaceGraph(Q3DSurface *surface, void *gmtApi, GMT_GRID *gmtGrid)
  : m_graph(surface), m_gmtGrid(gmtGrid)
{
  float areaWidth = gmtGrid->header->wesn[1] - gmtGrid->header->wesn[0];
  float areaHeight = gmtGrid->header->wesn[3] - gmtGrid->header->wesn[2];
  
    m_graph->setAxisX(new QValue3DAxis);
    m_graph->setAxisY(new QValue3DAxis);
    m_graph->setAxisZ(new QValue3DAxis);

    const QString *axisTitle = nullptr;

    float ewRange = gmtGrid->header->wesn[1] - gmtGrid->header->wesn[0];
    float nsRange = gmtGrid->header->wesn[3] - gmtGrid->header->wesn[2];

    // E-W axis
    axisTitle = new QString(gmtGrid->header->x_units);
    m_graph->axisX()->setTitle(*axisTitle);

    if (ewRange > 50) {
        m_graph->axisX()->setLabelFormat("%i");
      }
    else if (ewRange > 10) {
        m_graph->axisX()->setLabelFormat("%.1f");
      }
    else {
        m_graph->axisX()->setLabelFormat("%.4f");
      }

    // N-S axis
    axisTitle = new QString(gmtGrid->header->y_units);
    m_graph->axisZ()->setTitle(*axisTitle);

    if (nsRange > 50) {
        m_graph->axisZ()->setLabelFormat("%i");
      }
    else if (nsRange > 10) {
        m_graph->axisZ()->setLabelFormat("%.1f");
      }
    else {
        m_graph->axisZ()->setLabelFormat("%.4f");
      }

    // Elevation/depth title and units
    axisTitle = new QString(gmtGrid->header->z_units);
    m_graph->axisY()->setTitle(*axisTitle);

    // Elevation/depth axis
    m_graph->axisY()->setLabelFormat("%i");

    m_graph->axisX()->setRange(gmtGrid->header->wesn[0],
                               gmtGrid->header->wesn[1]);

    m_graph->axisZ()->setRange(gmtGrid->header->wesn[2],
			       gmtGrid->header->wesn[3]);
    
    m_graph->axisY()->setRange(gmtGrid->header->z_min,
			       gmtGrid->header->z_max +
			       fabsf(0.1 * gmtGrid->header->z_max));
    
    m_graph->axisZ()->setLabelAutoRotation(30);
    m_graph->axisX()->setLabelAutoRotation(90);
    m_graph->axisY()->setLabelAutoRotation(30);
    m_graph->activeTheme()->setType(Q3DTheme::ThemePrimaryColors);

    QFont font = m_graph->activeTheme()->font();
    font.setPointSize(20);
    m_graph->activeTheme()->setFont(font);

    m_topography = new TopographicSeries(gmtGrid);
    m_topography->setTopography(gmtApi, gmtGrid, areaWidth, areaHeight);
    m_topography->setItemLabelFormat(QStringLiteral("@yLabel m"));

    m_highlight = new HighlightSeries();
    m_highlight->setTopographicSeries(m_topography);
    // float minRange = areaWidth * 0.49f;
     float minRange = areaWidth * 0.1f;
     float aspectRatio = 0.1389f;
    m_highlight->setMinHeight(minRange * aspectRatio);
    m_highlight->setMinHeight(50.f);

    m_highlight->handleGradientChange(areaWidth * aspectRatio);
//! [1]
    QObject::connect(m_graph->axisY(), &QValue3DAxis::maxChanged,
                     m_highlight, &HighlightSeries::handleGradientChange);
//! [1]

    m_graph->addSeries(m_topography);
    m_graph->addSeries(m_highlight);

    m_inputHandler = new CustomInputHandler(m_graph);
    m_inputHandler->setHighlightSeries(m_highlight);
    m_inputHandler->setAxes(m_graph->axisX(), m_graph->axisY(), m_graph->axisZ());
    // m_inputHandler->setLimits(0.0f, areaWidth, minRange);
    m_inputHandler->setLimits(-10000.f, 10000.f, -10000.f);
    m_inputHandler->setAspectRatio(aspectRatio);

    m_graph->setActiveInputHandler(m_inputHandler);

    // No surface texture
    toggleSurfaceTexture(false);
}

SurfaceGraph::~SurfaceGraph()
{
    delete m_graph;
}

//! [0]
void SurfaceGraph::toggleSurfaceTexture(bool enable)
{
    if (enable)
        m_topography->setTextureFile(":/maps/maptexture");
    else
        m_topography->setTextureFile("");
}
//! [0]

void SurfaceGraph::adjustXMin(int min)
{
    float minX = m_stepX * float(min) + m_rangeMinX;

    int max = m_axisMaxSliderX->value();
    if (min >= max) {
        max = min + 1;
        m_axisMaxSliderX->setValue(max);
    }
    float maxX = m_stepX * max + m_rangeMinX;

    setAxisXRange(minX, maxX);
}

void SurfaceGraph::adjustXMax(int max)
{
    float maxX = m_stepX * float(max) + m_rangeMinX;

    int min = m_axisMinSliderX->value();
    if (max <= min) {
        min = max - 1;
        m_axisMinSliderX->setValue(min);
    }
    float minX = m_stepX * min + m_rangeMinX;

    setAxisXRange(minX, maxX);
}

void SurfaceGraph::adjustZMin(int min)
{
    float minZ = m_stepZ * float(min) + m_rangeMinZ;

    int max = m_axisMaxSliderZ->value();
    if (min >= max) {
        max = min + 1;
        m_axisMaxSliderZ->setValue(max);
    }
    float maxZ = m_stepZ * max + m_rangeMinZ;

    setAxisZRange(minZ, maxZ);
}

void SurfaceGraph::adjustZMax(int max)
{
    float maxX = m_stepZ * float(max) + m_rangeMinZ;

    int min = m_axisMinSliderZ->value();
    if (max <= min) {
        min = max - 1;
        m_axisMinSliderZ->setValue(min);
    }
    float minX = m_stepZ * min + m_rangeMinZ;

    setAxisZRange(minX, maxX);
}

void SurfaceGraph::setAxisXRange(float min, float max)
{
    m_graph->axisX()->setRange(min, max);
}

void SurfaceGraph::setAxisZRange(float min, float max)
{
    m_graph->axisZ()->setRange(min, max);
}
