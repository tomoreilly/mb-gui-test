#include <QVTKOpenGLNativeWidget.h>
#include <vtkActor.h>
#include <vtkPolyDataMapper.h>
#include <vtkDoubleArray.h>
#include <vtkGenericOpenGLRenderWindow.h>
#include <vtkPointData.h>
#include <vtkProperty.h>
#include <vtkRenderer.h>
#include <vtkElevationFilter.h>

#include <QApplication>
#include <QDockWidget>
#include <QGridLayout>
#include <QLabel>
#include <QMainWindow>
#include <QPointer>
#include <QPushButton>
#include <QVBoxLayout>

#include <cmath>
#include <cstdlib>

#include "GmtGridReader.h"


namespace {
  /**
   * render grid in the window
   *
   * @param window the window to render to
   */
  void Render(vtkGenericOpenGLRenderWindow* window);
} // namespace

int main(int argc, char* argv[])
{
  if (argc != 2) {
    std::cerr << "Usage: <gridFile>" << std::endl;
    return -1;
  }

  char *gridFilename = argv[1];
  
  QSurfaceFormat::setDefaultFormat(QVTKOpenGLNativeWidget::defaultFormat());

  QApplication app(argc, argv);

  // main window
  QMainWindow mainWindow;
  mainWindow.resize(1200, 900);

  // control area
  QDockWidget controlDock;
  mainWindow.addDockWidget(Qt::LeftDockWidgetArea, &controlDock);

  QLabel controlDockTitle("Control Dock");
  controlDockTitle.setMargin(20);
  controlDock.setTitleBarWidget(&controlDockTitle);

  QPointer<QVBoxLayout> dockLayout = new QVBoxLayout();
  QWidget layoutContainer;
  layoutContainer.setLayout(dockLayout);
  controlDock.setWidget(&layoutContainer);

  QPushButton refreshButton;
  refreshButton.setText("Refresh");
  dockLayout->addWidget(&refreshButton);

  // render area
  QPointer<QVTKOpenGLNativeWidget> vtkRenderWidget =
    new QVTKOpenGLNativeWidget();
  mainWindow.setCentralWidget(vtkRenderWidget);

  // VTK part
  vtkNew<vtkGenericOpenGLRenderWindow> window;
  vtkRenderWidget->setRenderWindow(window.Get());

  vtkNew<mb_system::GmtGridReader> reader;

  reader->SetFileName ( gridFilename );
  std::cout << "*** reader->Update()" << std::endl;
  reader->Update();
  if (reader->GetErrorCode()) {
    std::cout << "Error during reader->Update(): " << reader->GetErrorCode()
              << std::endl;
    return EXIT_FAILURE;
  }

  double zMin, zMax;
  reader->zBounds(&zMin, &zMax);

  vtkAlgorithmOutput *port = nullptr;
   
  std::cout << "*** create elevationFilter" << std::endl;
  // Color data points based on z-value
  vtkSmartPointer<vtkElevationFilter> elevationFilter =
    vtkSmartPointer<vtkElevationFilter>::New();

  std::cout << "*** elevationFilter->SetInputConnection\n";
  elevationFilter->SetInputConnection(reader->GetOutputPort());

  elevationFilter->SetLowPoint(0, 0, zMin);
  elevationFilter->SetHighPoint(0, 0, zMax);
  std::cout << "zMin: " << zMin << ", zMax: " << zMax << std::endl;

  std::cout << "*** create mapper and set connection" << std::endl;
  vtkNew<vtkPolyDataMapper> mapper;
  mapper->SetInputConnection(elevationFilter->GetOutputPort());

  vtkNew<vtkActor> actor;
  actor->SetMapper(mapper);
  //  actor->GetProperty()->SetEdgeVisibility(true);
  // actor->GetProperty()->SetRepresentationToSurface();

  vtkNew<vtkRenderer> renderer;
  renderer->AddActor(actor);

  window->AddRenderer(renderer);

  // Render
  ::Render(window);

  // connect the buttons
  QObject::connect(&refreshButton, &QPushButton::released,
                   [&]() { ::Render(window); });

  mainWindow.show();

  return app.exec();
}

namespace {
  void Render(vtkGenericOpenGLRenderWindow* window) {
    window->Render();
  }
  
} // namespace
