
cmake_minimum_required(VERSION 3.12 FATAL_ERROR)

if(POLICY CMP0020)
  cmake_policy(SET CMP0020 NEW)
  cmake_policy(SET CMP0071 NEW)
endif()

PROJECT(mbGrdViz)

find_package(VTK COMPONENTS 
  CommonCore
  CommonDataModel
  FiltersSources
  GUISupportQt
  InteractionStyle
  RenderingContextOpenGL2
  RenderingCore
  RenderingFreeType
  RenderingGL2PSOpenGL2
  RenderingOpenGL2
  IOCore
  GUISupportQt
  RenderingQt
)

if(NOT VTK_FOUND)
  message(FATAL_ERROR "mbGrdViz: Unable to find the VTK build folder.")
endif()

if(NOT(TARGET VTK::GUISupportQt))
  message(FATAL_ERROR "mbGrdViz: VTK not built with Qt support.")
endif()

if(NOT DEFINED VTK_QT_VERSION)
  set(VTK_QT_VERSION 5)
endif()

set(qt_components Core Gui Quick Widgets)
if(${VTK_QT_VERSION} VERSION_GREATER_EQUAL 6)
  list(APPEND qt_components OpenGLWidgets)
endif()
list(SORT qt_components)
# We have ui files, so this will also bring in the macro:
#   qt5_wrap_ui or qt_wrap_ui from Widgets.
find_package(Qt${VTK_QT_VERSION} QUIET
  REQUIRED COMPONENTS ${qt_components}
)

message("Add build-utils/ subdirectory to module CMAKE_MODULE_PATH")
list(APPEND CMAKE_MODULE_PATH $ENV{MBSYSTEM_HOME}/build-utils/)
message("CMAKE_MODULE_PATH now: ${CMAKE_MODULE_PATH}")

find_package(GMT REQUIRED)
message("GMT_INCLUDE_DIRS: ${GMT_INCLUDE_DIRS}")
message("GMT_LIBRARIES: ${GMT_LIBRARIES}")

find_package(PROJ REQUIRED)
message("PROJ_INCLUDE_DIRS: ${PROJ_INCLUDE_DIRS}")
message("PROJ_LIBRARIES: ${PROJ_LIBRARIES}")

foreach(_qt_comp IN LISTS qt_components)
  list(APPEND qt_modules "Qt${VTK_QT_VERSION}::${_qt_comp}")
endforeach()

message (STATUS "VTK_VERSION: ${VTK_VERSION}, Qt Version: ${Qt${VTK_QT_VERSION}Widgets_VERSION}")

# Instruct CMake to run moc and uic automatically when needed.
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTOUIC ON)

message("MBSYSTEM_HOME: $ENV{MBSYSTEM_HOME}")

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_BINARY_DIR} ${GMT_INCLUDE_DIRS} $ENV{MBSYSTEM_HOME}/src/qt-guilib)

file(GLOB UI_FILES *.ui)
file(GLOB QT_WRAP *.h)
file(GLOB CXX_FILES *.cxx)

# For VTK versions greater than or equal to 8.90.0:
#  CMAKE_AUTOUIC is ON so we handle uic automatically for Qt targets.
#  CMAKE_AUTOMOC is ON so we handle moc automatically for Qt targets.

# Prevent a "command line is too long" failure in Windows.
set(CMAKE_NINJA_FORCE_RESPONSE_FILE "ON" CACHE BOOL "Force Ninja to use response files.")
# CMAKE_AUTOMOC in ON so the MOC headers will be automatically wrapped.
add_executable(mbGrdViz MACOSX_BUNDLE
  ${CXX_FILES} ${UISrcs} ${QT_WRAP}
)
if (Qt${VTK_QT_VERSION}Widgets_VERSION VERSION_LESS "5.11.0")
  message("qt5_use_modules ${qt_components}")
  qt5_use_modules(mbGrdViz ${qt_components})
else()
  message("link_libraries ${qt_modules}")
  target_link_libraries(mbGrdViz ${qt_modules} $ENV{MBSYSTEM_HOME}/build/src/qt-guilib/libMBGui.so)
endif()

set(LOCAL_LIBDIR $ENV{MBSYSTEM_HOME}/build/src)
target_link_libraries(mbGrdViz ${VTK_LIBRARIES} ${qt_modules} ${LOCAL_LIBDIR}/mbio/libmbio.so ${LOCAL_LIBDIR}/surf/libmbsapi.so ${LOCAL_LIBDIR}/bsio/libmbbsio.so ${GMT_LIBRARIES} ${PROJ_LIBRARIES})

### target_link_libraries(mbGrdViz ${VTK_LIBRARIES} ${qt_modules})

# vtk_module_autoinit is needed
vtk_module_autoinit(
  TARGETS mbGrdViz
  MODULES ${VTK_LIBRARIES}
)
