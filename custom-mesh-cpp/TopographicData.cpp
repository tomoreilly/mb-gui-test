#include "TopographicData.h"
#include <QDebug>
#include <QString>
#include <QOpenGLShaderProgram>
#include <QVector3D>
#include <QResizeEvent>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include <float.h>

const float TopographicData::MinPosition = -5.;     // original -5.
const float TopographicData::PositionRange = 10.;   // original 10.

const char * TopographicData::PositionAttrName = "a_Position";
const char * TopographicData::NormalAttrName = "a_Normal";
const char * TopographicData::ColorAttrName = "a_Color";
const char * TopographicData::LightPositionName = "u_LightPos";
const char * TopographicData::MvpMatrixName = "u_MVPMatrix";
const char * TopographicData::MvMatrixName = "u_MVMatrix";

/* ***
float testVertices[] = {
  0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f, // top right
  0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom right
  -0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 0.0f, 1.0f, 1.0f, // bottom left
  -0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f   // top left
};
*** */

float testVertices[] = {
  501.5f,  501.5f, -1000.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f, // top right
  501.5f, 500.5f, -1000.0f, 0.f, 0.f, 0.f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom right
  500.5f, 500.5f, -1000.0f, 0.f, 0.f, 0.f, 0.0f, 0.0f, 1.0f, 1.0f, // bottom left
  500.5f,  501.5f, -1000.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f   // top left
};

GLuint testIndices[] = {  // note that we start from 0!
                          0, 1, 3,   // first triangle
                          1, 2, 3    // second triangle
                       };

TopographicData::TopographicData() :
  m_ibo(QOpenGLBuffer::IndexBuffer),
  m_gridVertices(nullptr), m_gridIndices(nullptr),
  m_nGridVertices(0), m_nGridIndices(0),
  m_testData(false)
{

}


void TopographicData::initializeMatrices() {

  qDebug("initializeMatrices()");
  return;
}


void TopographicData::setData(void *gmtApi, GMT_GRID *gmtGrid) {

  struct GMT_GRID_HEADER *header = gmtGrid->header;

  m_nGridVertices = header->n_columns * header->n_rows;

  // Create vertices for grid data
  m_gridVertices = (GLfloat *)malloc(m_nGridVertices * m_floatsPerVertex * sizeof(GLfloat));

  float zRange = 0.;
  // Get zRange and zMin for coloring algorithm; clip at z=0
  if (header->z_max > 0) {
    zRange = (0 - header->z_min);    
  }
  else {
    zRange = (header->z_max - header->z_min);
  }
  float zMin = header->z_min;

  float zPrev = 0.f;
  int vIndex = 0;   // Index into m_gridVertices array
  // Populate vertices with GMT_GRID data
  for (uint row = 0; row < header->n_rows; row++) {
      for (uint col = 0; col < header->n_columns; col++) {
          uint dataInd = GMT_Get_Index(gmtApi, gmtGrid->header, row, col);
          float z = gmtGrid->data[dataInd];
          if (isnanf(z)) {
              z = zPrev;  // Extrapolate previous value
            }

          m_gridVertices[vIndex++] = (float )gmtGrid->x[col];
          m_gridVertices[vIndex++] = (float )gmtGrid->y[row];
          m_gridVertices[vIndex++] = z;
          zPrev = z;

          // Estimate normal vector at this point, based on slope
          if (col > 0 && row > 0) {
              // Get z at (prev-x, y)
              uint prevInd = GMT_Get_Index(gmtApi, gmtGrid->header, row, col-1);
              float slopeX = (z - gmtGrid->data[prevInd]) / (float )(gmtGrid->x[col] - gmtGrid->x[col-1]);

              // Get z at (x, prev-y)
              prevInd = (row - 1) * header->n_rows + col;
              prevInd = GMT_Get_Index(gmtApi, gmtGrid->header, row-1, col);
              float slopeY = (z - gmtGrid->data[prevInd]) / (float )(gmtGrid->y[row] - gmtGrid->y[row-1]);

              // Normal vector is cross-product of slopeX ad slopeY
              const float planeVecX[] = {1.f, 0., slopeX};
              const float planeVecY[] = {0., 1.f, slopeY};

              float normVec[] =
              {(planeVecX[1] * planeVecY[2] - planeVecX[2] * planeVecY[1]),
               (planeVecX[2] * planeVecY[0] - planeVecX[0] * planeVecY[2]),
               (planeVecX[0] * planeVecY[1] - planeVecX[1] * planeVecY[0])};

              // Normalize the normal vector
              float length = sqrt(normVec[0]*normVec[0] +
                   normVec[1]*normVec[1] + normVec[2]*normVec[2]);

               m_gridVertices[vIndex++] = normVec[0] / length;
               m_gridVertices[vIndex++] = normVec[1] / length;
               m_gridVertices[vIndex++] = normVec[2] / length;
            }
          else {  // Point along x=0 or y=0 edge
              // Assume some slope for points at edge of grid
              m_gridVertices[vIndex++] = 0.f;
              m_gridVertices[vIndex++] = 0.f;
              m_gridVertices[vIndex++] = 0.f;
            }

          /* ***
          // Set normals to 0,0,0 for now
          m_gridVertices[vIndex++] = 0.f;
          m_gridVertices[vIndex++] = 0.f;
          m_gridVertices[vIndex++] = 0.f;
          *** */

          float red = 1.f, green = 0.f, blue = 0.f;
          // Assign color based on z
          setColor(z, zMin, zRange, &red, &green, &blue);  // Return white for now!
          m_gridVertices[vIndex++] = red;  // red
          m_gridVertices[vIndex++] = green;  // green
          m_gridVertices[vIndex++] = blue;  // blue
          m_gridVertices[vIndex++] = 1.f;  // alpha
          // fprintf(stderr, "red: %.3f, green: %.1f, blue: %.1f :::", red, green, blue);
        }
    }

  qDebug() << "m_nGridVertices: " << m_nGridVertices << ", vIndex: " << vIndex;

  /* ***
  // DEBUG - print out vertices
  fprintf(stderr, "vertices:\n");
  for (int i = 0; i < m_nGridVertices * m_floatsPerVertex; i++) {
      if (!(i%10)) {
          fprintf(stderr, "\n");
        }
      fprintf(stderr, "%.4f ", m_gridVertices[i]);
    }
  fprintf(stderr, "\n");
  *** */

  // Index the triangles to be drawn
  // Specify two triangles (6 vertices) starting at each row, col
  m_nGridIndices = 6 * (header->n_rows) * (header->n_columns);
  qDebug() << "m_nGridIndices: " << m_nGridIndices;
  // Allocate triangle indices
  m_gridIndices = (GLuint *)malloc(m_nGridIndices * sizeof(GLuint));
  GLuint *indices = m_gridIndices;

  int gIndex = 0;
  for (uint row = 0; row < header->n_rows - 1; row++) {
      for (uint col = 0; col < header->n_columns - 1; col++) {

          // First triangle
          indices[gIndex++] = vertexIndex(col, row, header->n_columns);
          indices[gIndex++] = vertexIndex(col+1, row, header->n_columns);
          indices[gIndex++] = vertexIndex(col+1, row+1, header->n_columns);

          // Second triangle
          indices[gIndex++] = vertexIndex(col, row, header->n_columns);
          indices[gIndex++] = vertexIndex(col+1, row+1, header->n_columns);
          indices[gIndex++] = vertexIndex(col, row+1, header->n_columns);
          // qDebug() << "gIndex: " << gIndex;
        }
    }

  qDebug() << "m_nGridIdices: " << m_nGridIndices << ", gIndex: " << gIndex;
  m_nGridIndices = gIndex;  // Adjust the number of actual vertices in the index
  qDebug("Done with setData()");
}

GLuint TopographicData::vertexIndex(int col, int row, int nColumns) {
  return (GLuint)( row * nColumns + col );
}



GMT_GRID *TopographicData::readGridFile(const char *gridFile, void **gmtApi) {

  // Check for file existence and readability
  struct stat fileStatus;

  if (stat(gridFile, &fileStatus) != 0
      || (fileStatus.st_mode & S_IFMT) == S_IFDIR
      || fileStatus.st_size <= 0) {
      qCritical() << "Can not read \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile: %s\n", gridFile);
  // Create GMT API
  *gmtApi =
      GMT_Create_Session("Topography::loadGrid()", 2U, 0U, nullptr);

  if (!*gmtApi) {
      qCritical() << "Could not get GMT API for \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile now: %s\n", gridFile);

  GMT_GRID *gmtGrid = nullptr;
  
  // Try to read header and grid
  for (int nTry = 0; !gmtGrid && nTry < 100; nTry++) {
      gmtGrid = (struct GMT_GRID *)GMT_Read_Data(*gmtApi, GMT_IS_GRID,
						 GMT_IS_FILE, GMT_IS_SURFACE,
						 GMT_GRID_ALL, nullptr,
						 gridFile, nullptr);
      if (gmtGrid) break;
      usleep(1000);
    }

  if (!gmtGrid) {
      qCritical() << "Unable to read GMT grid from \"" << gridFile << "\"";
      return nullptr;
    }
  return gmtGrid;
}


void TopographicData::setColor(float z, float zmin, float zrange, float *red, float *green, float *blue) {
  float ratio = (z - zmin) / zrange;
  *red = 1. - ratio;
  *blue = ratio;
  *green = 0.f;

  // TEST
  /***
  *red = 1.f;
  *green = 1.f;
  *blue = 1.f;
  * ***/
}


/// Get map limits
void TopographicData::getLimits(float *xMin, float *xMax, float *yMin, float *yMax, float *zMin, float *zMax) {

  *xMin = FLT_MAX;
  *xMax = -FLT_MAX;
  *yMin = FLT_MAX;
  *yMax = -FLT_MAX;
  *zMin = FLT_MAX;
  *zMax = -FLT_MAX;

  if (m_testData) {

      for (uint i = 0; i < sizeof(testVertices)/sizeof(float); i += m_floatsPerVertex) {
          if (testVertices[i] < *xMin) {
              *xMin = testVertices[i];
            }
          if (testVertices[i] > *xMax) {
              *xMax = testVertices[i];
            }
          if (testVertices[i+1] < *yMin) {
              *yMin = testVertices[i+1];
            }
          if (testVertices[i+1] > *yMax) {
              *yMax = testVertices[i+1];
            }
          if (testVertices[i+2] < *zMin) {
              *zMin = testVertices[i+2];
            }
          if (testVertices[i+2] > *zMax) {
              *zMax = testVertices[i+2];
            }
        }
    }
  else {
      *xMin = m_gmtGrid->header->wesn[0];
      *xMax = m_gmtGrid->header->wesn[1];
      *yMin = m_gmtGrid->header->wesn[2];
      *yMax = m_gmtGrid->header->wesn[3];
      *zMin = m_gmtGrid->header->z_min;
      *zMax = m_gmtGrid->header->z_max;
    }
}
