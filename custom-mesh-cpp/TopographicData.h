#ifndef TOPOGRAPHICDATA_H
#define TOPOGRAPHICDATA_H

#include <QOpenGLWindow>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QMatrix4x4>
#include <gmt/gmt.h>

class QOpenGLShaderProgram;

class TopographicData 
{

public:
  TopographicData();

  /// Read grid from GMT file; return pointer to GMT_GRID if successful,
  /// else return nullptr
  static GMT_GRID *readGridFile(const char *filename, void **gmtApi);

  /// Return grid vertices
  GLfloat *vertices() {
    return m_gridVertices;
  }

  /// Return number of grid vertices
  uint nVertices() {
    return m_nGridVertices;
  }

  /// Return indices of grid data
  GLuint *indices() {
    return m_gridIndices;
  }

  /// Return nummber of grid indices
  uint nIndices() {
    return m_nGridIndices;
  }
  
  /// Set topo grid vertices
  void setData(void *gmtApi, GMT_GRID *gmtGrid);
					       
private slots:


 protected:

  /// Get map limits, either from testVertices or from GMT grid
  void getLimits(float *xMin, float *xMax, float *yMin, float *yMax, float *zMin, float *zMax);

  /// Set color based on depth as fraction of grid depth range
  void setColor(float z, float zmin, float zrange, float *red, float *green, float *blue);


  /// Return offset into row-major data array at row and col with each row containing nColumns
  static GLuint vertexIndex(int col, int row, int nColumns);

  /// Initialize matrices
  void initializeMatrices();

  /// GMT API pointer
  void *m_gmtApi = nullptr;

  /// GMT grid (read from GMT file)
  struct GMT_GRID *m_gmtGrid = nullptr;

  /// Shaders
  QOpenGLShaderProgram *m_program;

  /// VAO
  QOpenGLVertexArrayObject m_vao;

  /// VBO
  QOpenGLBuffer m_vbo;

  /// IBO
  QOpenGLBuffer m_ibo;

  /// Size of the viewport
  QSize m_viewportSize;

  /// Size characteristics of parabola height map
  static const int VerticesPerSide = 32;   // was 32
  /// x, y, z
  static const int m_nPositionTuples = 3;
  /// x, y, z
  static const int m_nNormalTuples = 3;
  /// R, G, B, Alpha
  static const int m_nColorTuples = 4;

  static const int m_floatsPerVertex = (m_nPositionTuples + m_nNormalTuples + m_nColorTuples);

  /// Model matrix; moves model from object space to world space
  QMatrix4x4 m_modelMatrix;

  /// Projection matrix; projects scene onto viewport
  QMatrix4x4 m_projectionMatrix;

  /// View matrix; "camera", transforms world space to eye space
  QMatrix4x4 m_viewMatrix;

  /// Light model matrix
  QMatrix4x4 m_lightModelMatrix;

  /// Combined matrix
  QMatrix4x4 m_mvpMatrix;

  /// Current rotation matrix
  QMatrix4x4 m_rotationMatrix;

  /// Accumulated rotation matrix
  QMatrix4x4 m_accumRotationMatrix;

  /// Temporary matrix
  QMatrix4x4 m_tempMatrix;

  /// Attributes for shaders
  static const char *PositionAttrName;
  int m_positionAttr = 0;
  static const char *NormalAttrName;
  int m_normalAttr = 0;
  static const char *ColorAttrName;
  int m_colorAttr = 0;

  /// Shader matrix "uniform" variables
  static const char *LightPositionName;
  static const char *MvpMatrixName;
  static const char *MvMatrixName;

  static const int Stride =
      sizeof(float) * (m_nPositionTuples + m_nNormalTuples + m_nColorTuples);

  static const float MinPosition;
  static const float PositionRange;

  GLfloat *m_gridVertices;
  GLuint *m_gridIndices;

  /// Number of elements in m_mapVertices array
  int m_nGridVertices;

  /// Number of elements in m_mapIndexData array
  int m_nGridIndices;

  bool m_testData;


};

#endif 
