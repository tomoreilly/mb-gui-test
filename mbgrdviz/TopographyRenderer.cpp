#include <QQuickWindow>
#include <sys/stat.h>
#include <unistd.h>
#include <math.h>
#include "TopographyRenderer.h"

float testVertices[] = {
  0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f, // top right
  0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom right
  -0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 0.0f, 1.0f, 1.0f, // bottom left
  -0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f   // top left
};

GLuint testIndices[] = {  // note that we start from 0!
                          0, 1, 3,   // first triangle
                          1, 2, 3    // second triangle
                       };


const char * TopographyRenderer::m_positionAttrName = "a_Position";
const char * TopographyRenderer::m_normalAttrName = "a_Normal";
const char * TopographyRenderer::m_colorAttrName = "a_Color";
const char * TopographyRenderer::LightPositionName = "u_LightPos";

TopographyRenderer::TopographyRenderer() : m_testData(true) {
}

TopographyRenderer::~TopographyRenderer() {
  m_ibo.destroy();
  m_vbo.destroy();
  m_vao.destroy();
}


bool TopographyRenderer::initialize(Topography *topography) {

  initializeOpenGLFunctions();
  m_topography = topography;

  if (!topography->m_gmtGrid) {
      qCritical() << "Topography has no GMT Grid data yet";
      return false;
    }

  setData(topography);

  glClearColor(1.0f, 0.0f, 1.0f, 1.0f);

  const char *vertexShaderFile = ":/shaders/test.vert";
  const char *fragShaderFile = ":/shaders/test.frag";

  // Create shader program, add shaders, link and bind.
  m_program = new QOpenGLShaderProgram();
  if (!m_program->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                          vertexShaderFile)) {
      // Error
      QString msg = m_program->log();
      qCritical() << "Error adding fragment shader from " << vertexShaderFile
                  << "\":\n" + msg.toLatin1();
      return false;
    }
  qDebug() << "Added vertex shader file " << vertexShaderFile;

  if (!m_program->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                          fragShaderFile)) {
      // Error
      QString msg = m_program->log();
      qCritical() << "Error adding fragment shader from " << fragShaderFile
                  << "\":\n" + msg.toLatin1();
      return false;
    }
  qDebug("added fragment shader");

  if (!m_program->link()) {
      // Error
      QString msg = m_program->log();
      qCritical() << "Error linking shaders:\n" + msg.toLatin1();
      return false;
    }
  qDebug("shaders linked ok");

  m_program->bind();

  // Create VAO
  if (!m_vao.create()) {
      // Error
      qCritical("Error creating vbo");
      return false;
    }

  // Bind VAO
  m_vao.bind();

  // Create, bind and allocate VBO and IBO
  m_vbo.create();
  m_vbo.bind();
  m_vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
  m_ibo.create();
  m_ibo.bind();
  m_ibo.setUsagePattern(QOpenGLBuffer::StaticDraw);

  if (m_testData) {
      m_vbo.allocate(testVertices, sizeof(testVertices));
      m_ibo.allocate(testIndices, sizeof(testIndices));
    }
  else {
      m_vbo.allocate(m_gridVertices, m_nGridVertices * sizeof(GLfloat));
      m_ibo.allocate(m_gridIndices, m_nGridIndices * sizeof(GLuint));
    }

  // Configure attributes for shader
  m_positionAttr = glGetAttribLocation(m_program->programId(), m_positionAttrName);
  m_normalAttr = glGetAttribLocation(m_program->programId(), m_normalAttrName);
  m_colorAttr = glGetAttribLocation(m_program->programId(), m_colorAttrName);
  fprintf(stderr, "positionAttr: %d, normalAttr: %d, colorAttr: %d\n",
          m_positionAttr, m_normalAttr, m_colorAttr);

  m_program->setAttributeBuffer(m_positionAttr, GL_FLOAT, 0,
                                m_nPositionTuples, Stride);

  m_program->setAttributeBuffer(m_normalAttr, GL_FLOAT,
                                m_nPositionTuples * sizeof(GL_FLOAT),
                                m_nNormalTuples, Stride);

  m_program->setAttributeBuffer(m_colorAttr, GL_FLOAT,
                                (m_nPositionTuples + m_nNormalTuples) *
                                sizeof(GL_FLOAT),
                                m_nColorTuples, Stride);

  m_program->enableAttributeArray(m_positionAttr);
  m_program->enableAttributeArray(m_normalAttr);
  m_program->enableAttributeArray(m_colorAttr);



  // Unbind VAO
  m_vao.release();
  m_vbo.release();
  m_program->release();

  m_initialized = true;
  qDebug("done with initialize()");
  return true;

}


void TopographyRenderer::setData(Topography *topography) {

  struct GMT_GRID *gmtGrid = topography->m_gmtGrid;
  struct GMT_GRID_HEADER *header = gmtGrid->header;


}

void TopographyRenderer::paint() {

  qDebug("TopographyRenderer::paint()");

  glViewport(0, 0, m_viewportSize.width(), m_viewportSize.height());

  glClear(GL_COLOR_BUFFER_BIT);
  m_program->bind();

  GLint model = glGetUniformLocation(m_program->programId(), "model");
   if (model == -1) {
       qCritical() << "variable \"" << model << "\" not found in shader";
       return;
     }

   GLint view = glGetUniformLocation(m_program->programId(), "view");
   if (view == -1) {
       qCritical() << "variable \"" << view << "\" not found in shader";
       return;
     }

   GLint projection = glGetUniformLocation(m_program->programId(), "projection");
   if (projection == -1) {
       qCritical() << "variable \"" << projection << "\" not found in shader";
       return;
     }

   GLint lightPos = glGetUniformLocation(m_program->programId(), LightPositionName);
   if (lightPos == -1) {
       qCritical() << "variable " << LightPositionName << " not found in shader";
       return;
     }

   if (m_testData) {
       // Set transforms for test data shape
       qDebug("Set transform to identity matrix");
       m_modelMatrix.setToIdentity();
       // Rotate model around axes
       m_modelMatrix.rotate(0.f, 1.0f, 0.f, 0.f);
       m_modelMatrix.rotate(0.f, 0.f, 1.0f, 0.f);

       // Move view back along z-axis
       m_viewMatrix.setToIdentity();
       m_viewMatrix.translate(0.f, 0.f, -10.0f);

       // Set projection matrix
       m_projectionMatrix.setToIdentity();
       m_projectionMatrix.perspective(45.f, 0.8, 0.1, 1000.f);

       qDebug("send modelMatrix to shader");
       glUniformMatrix4fv(model, 1, false, m_modelMatrix.constData());

       qDebug("send viewMatrix to shader");
       glUniformMatrix4fv(view, 1, false, m_viewMatrix.constData());

       qDebug("send projectionMatrix to shader");
       glUniformMatrix4fv(projection, 1, false, m_projectionMatrix.constData());

       // Set lighting
       m_lightModelMatrix.setToIdentity();
       m_lightModelMatrix.translate(0.f, 1.f, 5.0f);

       // m_modelLightPos(0.f, 0.f, 0.f, 1.0f);
       m_modelLightPos.setZ(1.9f);
       m_worldLightPos = m_lightModelMatrix * m_modelLightPos;
       m_eyeLightPos = m_viewMatrix * m_worldLightPos;

       qDebug("send eye-light-position to shader");
       glUniform3f(lightPos, m_worldLightPos[0], m_worldLightPos[1], m_worldLightPos[2]);

     }
   else {
       qDebug("Set transform to identity matrix");
       m_modelMatrix.setToIdentity();

       m_modelMatrix.rotate(-55.f, 1.0f, 0.f, 0.f);
       m_modelMatrix.rotate(-45.f, 0.f, 1.0f, 0.f);
       m_viewMatrix.setToIdentity();
       // Move back along z-axis
       m_viewMatrix.translate(0.f, 0.f, -3.0f);

       m_projectionMatrix.setToIdentity();
       m_projectionMatrix.perspective(45.f, 0.8, 0.1, 1000.f);

       qDebug("send modelMatrix to shader");
       glUniformMatrix4fv(model, 1, false, m_modelMatrix.constData());

       qDebug("send viewMatrix to shader");
       glUniformMatrix4fv(view, 1, false, m_viewMatrix.constData());

       qDebug("send projectionMatrix to shader");
       glUniformMatrix4fv(projection, 1, false, m_projectionMatrix.constData());

       // Set lighting
       m_lightModelMatrix.setToIdentity();
       m_lightModelMatrix.translate(0.f, 5.5f, 15.0f);

       // m_modelLightPos(0.f, 0.f, 0.f, 1.0f);
       m_modelLightPos.setZ(1.9f);
       m_worldLightPos = m_lightModelMatrix * m_modelLightPos;
       m_eyeLightPos = m_viewMatrix * m_worldLightPos;

       qDebug("send eye-light-position to shader");
       glUniform3f(lightPos, m_worldLightPos[0], m_worldLightPos[1], m_worldLightPos[2]);
     }
   m_vao.bind();

  if (m_testData) {
      glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
  else {
      // Draw from vertex index buffer
      glDrawElements(GL_TRIANGLE_STRIP, m_nGridIndices, GL_UNSIGNED_INT, 0);
    }
  m_vao.release();

  m_program->release();
  glFlush();

  /* ***
  if (m_topography->window()) {
      m_topography->window()->update();
    }
*** */
  return;

}




