#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QQmlComponent>

#include "backend.h"
#include "squircle.h"
#include "Topography.h"

QQuickWindow *g_rootWindow;
QQmlApplicationEngine *g_appEngine;

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    qmlRegisterSingletonType<BackEnd>("mbgrdviz.backend", 1, 0, "BackEnd", BackEnd::qmlInstance);
    qmlRegisterType<Squircle>("OpenGLUnderQML", 1, 0, "Squircle");
    qmlRegisterType<Topography>("mbgrdviz.Topography", 1, 0, "Topography");

    // QQmlApplicationEngine engine;
    g_appEngine = new QQmlApplicationEngine();

    g_appEngine->load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (g_appEngine->rootObjects().isEmpty())
        return -1;

    g_rootWindow = qobject_cast<QQuickWindow*>(g_appEngine->rootObjects().value(0));
    qDebug("call app.exec");
     return app.exec();
}
