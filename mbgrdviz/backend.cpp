#include <stdio.h>
#include <string.h>
#include <QQmlComponent>
#include <QQmlProperty>
#include <QQmlApplicationEngine>
#include <QtDebug>
#include <QObject>
#include <QQuickItem>
#include <QByteArray>
#include <QtGui>
#include "backend.h"
#include "Topography.h"

#include <mb_define.h>
#include "MbAux.h"


BackEnd::BackEnd(QObject *parent): QObject(parent), m_topography(nullptr)
{
    qDebug("BackEnd::BackEnd()");
    // Find topography window
    QObject *object = g_rootWindow->findChild<QObject *>("topography");
    if (!object) {
        qCritical() << "Couldn't find \"topography\" object in GUI";
      }
    else {
        qDebug("Found topography object in GUI");
      }
    // TEST TEST TEST
    object = g_rootWindow->findChild<QObject *>("2dXOffset");
    if (!object) {
        qDebug() << "Couldn't find 2dXOffset object";
      }
    else {
        qDebug() << "Found 2dXOffset object";
      }
}

QString BackEnd::yOffset2d() {
    qDebug("BackEnd::yOffset2d() - " + m_yOffset2d.toLatin1());
    return m_yOffset2d;
}

void BackEnd::setyOffset2d(const QString &yOffset2d) {
    qDebug("BackEnd::setyOffset2d() - " + yOffset2d.toLatin1());
    if (yOffset2d == m_yOffset2d) {
        return;
    }

    m_yOffset2d = yOffset2d;
    emit yOffset2dChanged();
}

void BackEnd::settings2dUpdated() {
    qDebug("BackEnd::twodSettingsUpdated()");

    fprintf(stderr, "now g_rootWindow: %p\n", g_rootWindow);

    bool ok = false;
    bool error = false;
    QObject *object = g_rootWindow->findChild<QObject *>("2dXOffset");
    int xoffset = intTextFieldValue(object, ok);
    if (ok) {
        fprintf(stderr, "xoffset: %d\n", xoffset);
    }
    else {
        error = true;
        fprintf(stderr, "invalid xoffset: must be integer");
        popupMessage("invalid xoffset: must be integer");
        return;
    }

    object = g_rootWindow->findChild<QObject *>("2dYOffset");
    int yoffset = intTextFieldValue(object, ok);
    if (ok) {
        fprintf(stderr, "yoffset: %d\n", yoffset);
    }
    else {
        error = true;
        fprintf(stderr, "invalid yoffset: must be integer");
        popupMessage("invalid yoffset: must be integer");
        return;
    }

    object = g_rootWindow->findChild<QObject *>("2dZoom");
    int zoom = intTextFieldValue(object, ok);
    if (ok) {
        fprintf(stderr, "zoom: %d\n", zoom);
    }
    else {
        error = true;
        fprintf(stderr, "invalid zoom: must be integer");
        popupMessage("invalid zoom: must be integer");
        return;
    }

    // Modify map display per new settings
    fprintf(stderr, "Modify map display per new settigns\n");
}

void BackEnd::settings3dUpdated() {
    qDebug("BackEnd::threedSettingsUpdated()");
}


int BackEnd::intTextFieldValue(QObject *object, bool &ok) {
    if (!object) {
        fprintf(stderr, "Null object argument\n");
        ok = false;
        return 0;
    }

    qDebug() << "intTextFieldValue(): " << QQmlProperty::read(object, "text").toString();
    int value = QQmlProperty::read(object, "text").toInt(&ok);
    if (ok) {
        return value;
    }
    else {
        return 0;
    }
}

int BackEnd::floatTextFieldValue(QObject *object, bool &ok) {
    if (!object) {
        fprintf(stderr, "Null object argument\n");
        ok = false;
        return 0;
    }

    qDebug() << "intTextFieldValue(): " << QQmlProperty::read(object, "text").toString();
    int value = QQmlProperty::read(object, "text").toInt(&ok);
    if (ok) {
        return value;
    }
    else {
        return 0;
    }
}

void BackEnd::popupMessage(const char *msg) {
    qDebug("popupMessage()");
    QObject *obj = g_rootWindow->findChild<QObject *>("myMessageDialog");
    if (obj) {
        obj->setProperty("visible", true);
        obj->setProperty("title", "got an error");
        obj->setProperty("text", msg);
    }
    else {
        fprintf(stderr, "popupMessage(): Couldn't find myMessageDialog\n");
    }
}

int BackEnd::openGridFile(const QString &filename) {
    qDebug() << "open file " << filename;

    const char *fname = filename.toLatin1().data();
    if (!strncmp(fname, "file://", strlen("file://"))) {
        fname += strlen("file://");
    }

    // Find topography window
    QObject *object = g_rootWindow->findChild<QObject *>("topography");
    if (!object) {
        qCritical() << "Couldn't find \"topography\" object in GUI";
        return -1;
      }
    else {
        qDebug("Found topography object in GUI");
        m_topography = (Topography *)object;
      }


    char buf[256];
    if (!m_topography->loadTopoGrid(fname)) {
        sprintf(buf, "Error reading grid file %s", fname);
        popupMessage(buf);
        return -1;
      }
    else {
        popupMessage("Opened grid file");
      }

    return 0;
}



