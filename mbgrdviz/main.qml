import QtQuick 2.9
import QtQuick.Controls 2.3
import QtQuick.Dialogs 1.1
import MbSystem.BackEnd 1.0
import OpenGLUnderQML 1.0

/* ***
BackEnd singleton is registered in root context by main.cpp
See https://qml.guide/singletons/
*** */

import "ui-components"

ApplicationWindow {
    id: appWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("mbgrdviz")
    Component.onCompleted: {
        BackEnd.yOffset2d = "hi there"; console.log("completed appWindow creation")
    }

    Settings2dWindow {
        id: settings2d
        visible: false
    }

    Settings3dWindow {
        id: settings3d
        visible: false
    }

    ActionGroup {
        id: mapActions
        exclusive: true
    }

    ActionGroup {
        id: topoActions
        exclusive: true
    }

    ActionGroup {
        id: shadeActions
        exclusive: true
    }

    ActionGroup {
        id: navActions
        exclusive: true
    }

    ActionGroup {
        id: colorActions
        exclusive: true
    }

    ActionGroup {
        id: mouseActions
        exclusive: true
    }

    menuBar: MenuBar {
        Menu {
            title: qsTr("&File")
            Action { text: qsTr("&Open primary grid...") ; onTriggered: {
                    fileDialog.title = qsTr("Open primary grid")
                    console.log("show file dialog now!");
                    fileDialog.open()
                }
            }

            Action { text: qsTr("&Open overlay grid...") }
            Action { text: qsTr("&Open site...") }
            Action { text: qsTr("&Open route...") }
            Action { text: qsTr("&Open navigation..."); onTriggered: {
                    console.log("show file dialog");
                    fileDialog.open()
                } }
            MenuSeparator { }
            Action { text: qsTr("&Exit"); onTriggered: {
                    console.log("quitting"); quitDialog.open() // Qt.quit(0)
                }
            }
        }
        Menu {
            title: qsTr("&View")
            Menu {
                title: "Map/3D"
                Action { checkable: true; checked: true; text: qsTr("&Map"); ActionGroup.group: mapActions }
                Action { checkable: true; text: qsTr("&3D"); ActionGroup.group: mapActions }
                Action { checkable: true; checked: true; text: qsTr("&Topography"); ActionGroup.group: topoActions }
                Action { checkable: true; text: qsTr("&Topography slope"); ActionGroup.group: topoActions }

            }

            MenuSeparator {}
            Action { checkable: true; text: qsTr("&Histograms") }
            Action { checkable: true; text: qsTr("&Contours") }
            Action { checkable: true; text: qsTr("&Sites") }
            Action { checkable: true; text: qsTr("&Routes") }
            Action { checkable: true; text: qsTr("&Vector") }
            Action { checkable: true; text: qsTr("&Profile window") }
            MenuSeparator {}
            Menu {
                title: "Shading"
                Action {checkable: true; checked: true; text: qsTr("Off"); ActionGroup.group: shadeActions }
                Action {checkable: true; text: qsTr("Slope"); ActionGroup.group: shadeActions}
                Action {checkable: true; text: qsTr("Illumination"); ActionGroup.group: shadeActions }
            }
            MenuSeparator {}
            Menu {
                title: "Navigation"
                Action {checkable: true; checked: true; text: qsTr("Off"); ActionGroup.group: navActions }
                Action {checkable: true; text: qsTr("Draped"); ActionGroup.group: navActions}
                Action {checkable: true; text: qsTr("Non-draped"); ActionGroup.group: navActions }
            }
            MenuSeparator {}
            Menu {
                title: "Color table"
                Action {checkable: true; checked: true; text: qsTr("Haxby"); ActionGroup.group: colorActions }
                Action {checkable: true; text: qsTr("Bright rainbow"); ActionGroup.group: colorActions}
                Action {checkable: true; text: qsTr("Muted rainbow"); ActionGroup.group: colorActions }
                Action {checkable: true; text: qsTr("Grayscale"); ActionGroup.group: colorActions }
                Action {checkable: true; text: qsTr("Flat gray"); ActionGroup.group: colorActions }
                Action {checkable: true; text: qsTr("Sealevel1"); ActionGroup.group: colorActions }
                Action {checkable: true; text: qsTr("Sealevel2"); ActionGroup.group: colorActions }
            }
        }
        Menu {
            title: "&Settings"
            Action {text: qsTr("Color and contours"); onTriggered: {
                    console.log("Create Popup");
                    var component = Qt.createComponent("ui-components/Popup.qml");
                    if (component.status === Component.Ready) {
                        console.log("component is ready - yOffset2d=" + BackEnd.yOffset2d);
                        var dialog = component.createObject(appWindow,{popupType: 1});
                        // dialogConnection.target = dialog
                        dialog.show();
                    }
                    else {
                        console.log("component is NOT ready");
                    }
                }
            }

            Action {text: qsTr("2D"); onTriggered: {
                    console.log("show 2d settings window");
                    settings2d.show()
                }
            }

            Action {text: qsTr("3D"); onTriggered: {
                    console.log("show 3d settings window");
                    settings3d.show()
                }
            }
            Action {text: qsTr("Shading")}
            Action {text: qsTr("Resolution")}
            Action {text: qsTr("Projections")}
            Action {text: qsTr("Site list")}
            Action {text: qsTr("Route list")}
            Action {text: qsTr("Navigation list")}
        }
        Menu {
            title: "&Mouse"
            Action {checkable: true; checked: true; text: qsTr("Pan and zoom"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Rotate model"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Rotate view"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Shading"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Pick area"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Edit sites"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Edit routes"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Pick nav"); ActionGroup.group: mouseActions }
            Action {checkable: true; text: qsTr("Pick nav file"); ActionGroup.group: mouseActions }
        }

        Menu {
            title: "Help"
            Action {text: qsTr("About"); onTriggered: {
                    console.log("show version info");
                    myMessageDialog.text = qsTr("PROTOTYPE");
                    myMessageDialog.open()
                }

            }
        }

    }

    ScrollView {
        id: scrollView
        anchors.fill: parent

        Column {
            id: column
            width: 200
            anchors.bottom: parent.bottom
            anchors.top: parent.top

            Item {

                width: 320
                height: 480


                Squircle {
                    id: squircle
                    objectName: "squircle"
                    SequentialAnimation on t {
                        NumberAnimation { to: 1; duration: 2500; easing.type: Easing.Linear }
                        NumberAnimation { to: 0; duration: 2500; easing.type: Easing.Linear }
                        loops: Animation.Infinite
                        running: true
                    }

                }

             //   Topography {
               //     id: topography
                 //   objectName: "topography"

               // }

            }


            Text {
                id: picked
                text: qsTr("picked lon: xxx, lat:yyy,z:-zzz ")
                font.pixelSize: 12
            }

            Text {
                id: mouseMode
                text: qsTr("Mouse mode:")
                font.pixelSize: 12
            }
        }
    }

    MessageDialog {
        title: "Overwrite?"
        icon: StandardIcon.Question
        text: "file.txt already exists.  Replace?"
        detailedText: "To replace a file means that its existing contents will be lost. " +
                      "The file that you are copying now will be copied over it instead."
        standardButtons: StandardButton.Yes | StandardButton.YesToAll |
                         StandardButton.No | StandardButton.NoToAll | StandardButton.Abort
        Component.onCompleted: visible = false
        onYes: console.log("copied")
        onNo: console.log("didn't copy")
        onRejected: console.log("aborted")
    }

    FileDialog {
        id: fileDialog
        title: qsTr("Open file")
        nameFilters: ["Grid files (*.grd)", "All files (*)"]
        onAccepted: {
            console.log("Accepted " + fileUrl)
            BackEnd.openGridFile(fileUrl)
        }
    }

    MessageDialog {
        id: quitDialog
        title: "Quit?"
        icon: StandardIcon.Question
        text: "Quit application?"
        standardButtons: StandardButton.Yes |
                         StandardButton.No
        Component.onCompleted: visible = false
        onYes: Qt.quit(0)
        onNo: console.log("did not quit")
    }

    MessageDialog {
        id: myMessageDialog
        objectName: "myMessageDialog"
        title: "my message dialog"
        text: "this is default text"
        visible: false
    }


}

