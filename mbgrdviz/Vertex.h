#ifndef VERTEX_H
#define VERTEX_H

#include <QtGlobal>
#include <QVector3D>

class Vertex
{
public:
    // Constructors
    Q_DECL_CONSTEXPR Vertex() {}
    Q_DECL_CONSTEXPR explicit Vertex(const QVector3D &position) : m_position(position) {}
    Q_DECL_CONSTEXPR Vertex(const QVector3D &position, const QVector3D &color) :
        m_position(position), m_color(color) {}

    // Accessors / Mutators
    Q_DECL_CONSTEXPR const QVector3D& position() const { return m_position; }

    Q_DECL_CONSTEXPR const QVector3D& color() const { return m_color; }

     void setPosition(const QVector3D& position) { m_position = position; }

     void setColor(const QVector3D& color) { m_color = color; }

     // OpenGL Helpers
     static const int PositionTupleSize = 3;
     static const int ColorTupleSize = 3;
     static Q_DECL_CONSTEXPR int positionOffset() { return offsetof(Vertex, m_position); }
     static Q_DECL_CONSTEXPR int colorOffset() { return offsetof(Vertex, m_color); }
     static Q_DECL_CONSTEXPR int stride() { return sizeof(Vertex); }

   private:

     QVector3D m_position;
     QVector3D m_color;

};

/*******************************************************************************
 * Inline Implementation
 ******************************************************************************/

// Note: Q_MOVABLE_TYPE means it can be memcpy'd.
Q_DECLARE_TYPEINFO(Vertex, Q_MOVABLE_TYPE);

#endif // VERTEX_H
