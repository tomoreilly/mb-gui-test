#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QQmlEngine>
#include <QJSEngine>
#include <QQuickWindow>
#include <QQmlApplicationEngine>
#include "Topography.h"

class BackEnd: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString yOffset2d READ yOffset2d WRITE setyOffset2d NOTIFY yOffset2dChanged)
    Q_DISABLE_COPY(BackEnd)

public:
    explicit BackEnd(QObject *parent = nullptr);

    void setRootObject(QObject *rootObject);
    /// Return backend value
    QString yOffset2d();

    /// Set TextEdit value to backend value
    void setyOffset2d(const QString &yOffset2d);

    static QObject *qmlInstance(QQmlEngine *engine, QJSEngine *scriptEngine) {
        Q_UNUSED(engine);
        Q_UNUSED(scriptEngine)

        QObject *obj = new BackEnd();
        fprintf(stderr, "backend singleton ptr: %p\n", obj);

        return obj;
    }

    /// Return int value of specified text field object; set ok=true if successful conversion
    int intTextFieldValue(QObject *object, bool &ok);

    /// Return float value of specified text field object; set ok=true if successful conversion
    int floatTextFieldValue(QObject *object, bool &ok);

    /// Pop up a dialog with specified message
    void popupMessage(const char *msg);

    // Apply new 2D settings
    Q_INVOKABLE void settings2dUpdated();

    // Apply new 3D settings
    Q_INVOKABLE void settings3dUpdated();

    // open specified grid file; return 0 on success, -1 on error
    Q_INVOKABLE int openGridFile(const QString &filename);

signals:
    void yOffset2dChanged();

private:
    QString m_xOffset;
    QString m_yOffset2d;
    QString m_zoom;
    QQmlComponent *m_messageDialog;
    Topography *m_topography = nullptr;
};

extern QQuickWindow *g_rootWindow;
extern QQmlApplicationEngine *g_appEngine;
#endif
