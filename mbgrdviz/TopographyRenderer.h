#ifndef TOPOGRAPHYRENDERER_H
#define TOPOGRAPHYRENDERER_H

#include <QQuickItem>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <QOpenGLWidget>
#include <mb_define.h>
#include <gmt/gmt.h>
#include "MbAux.h"


class Topography;

/// TopographyRenderer is responsible for drawing topography, "lives" in the rendering thread so is
/// separate from Topography class which is a QQuickItem living in the GUI thread.
class TopographyRenderer : public QObject, protected QOpenGLFunctions {
    Q_OBJECT

public:
    TopographyRenderer();

    ~TopographyRenderer();

    /// Initialize with topography object; return true on success
    bool initialize(Topography *topography);

    void setViewportSize(const QSize &size) { m_viewportSize = size; }

    void setWindow(QQuickWindow *window) { m_window = window; }

    /// Return true if vertices have been loaded
    bool initialized() {
      return m_initialized;
    }

public slots:
    /// Draw grid
    void paint();

protected:

    /// Build vertices, indices from GMT topography grid
    void setData(Topography *topography);

    /// Shaders
    QOpenGLShaderProgram *m_program;

    /// Vertex array object
    QOpenGLVertexArrayObject m_vao;

    /// Vertex buffer object
    QOpenGLBuffer m_vbo;

    /// Index buffer object
    QOpenGLBuffer m_ibo;

    /// Attributes for shaders
    static const char *m_positionAttrName;
    int m_positionAttr = 0;
    static const char *m_normalAttrName;
    int m_normalAttr = 0;
    static const char *m_colorAttrName;
    int m_colorAttr = 0;

    /// Shader matrix "uniform" variables
    static const char *LightPositionName;
    static const char *MvpMatrixName;
    static const char *MvMatrixName;

    /// Model matrix; moves model from object space to world space
     QMatrix4x4 m_modelMatrix;

     /// Projection matrix; projects scene onto viewport
     QMatrix4x4 m_projectionMatrix;

     /// View matrix; "camera", transforms world space to eye space
     QMatrix4x4 m_viewMatrix;

     /// Light model matrix
     QMatrix4x4 m_lightModelMatrix;

     /// Temporary matrix
     QMatrix4x4 m_tempMatrix;

     /// Current light position in world space (after transformation
     /// via model matrix)
     QVector4D m_worldLightPos;

     /// Light position in eye space (after transformation via model-view
     /// matrix)
     QVector4D m_eyeLightPos;

     /// Light centered at model space origin
     QVector4D m_modelLightPos;

    /// Size of the viewport
    QSize m_viewportSize;

    /// QQuickWindow contains topography display, user controls
    QQuickWindow *m_window;

    /// Each point or "vertex" in topographic grid includes multiple floating point
    /// tuples, as follows:
    /// Three floats; x, y, z
    const static int m_nPositionTuples = 3;
    /// Three floats; x, y, z
    const static int m_nNormalTuples = 3;
    /// Three floats; r, g, b, alpha
    const static int m_nColorTuples = 4;

    /// Stride (bytes) between each vertex
    static const int Stride =
        sizeof(float) * (m_nPositionTuples + m_nNormalTuples + m_nColorTuples);


    /// Grid vertex array
    uint m_nGridVertices = 0;
    GLfloat *m_gridVertices = nullptr;

    /// Index array for grid vertices
    uint m_nGridIndices;
    GLuint *m_gridIndices;

    /// Grid vertices have been loaded
    bool m_initialized = false;

    /// Multiply grid file z-values by this number
    float m_zScaleFactor = 0.001f;

    /// Pointer to topography
    Topography *m_topography;

    bool m_testData;
};


#endif // TOPOGRAPHYRENDERER_H
