/// This file wraps headers from the original 'C' version of MB-System, not
/// all of which have an #ifndef include guard.
#ifndef MBAUX_H
#define MBAUX_H

extern "C" {
#include <mb_aux.h>
};

#endif
