import QtQuick 2.9
import QtQuick.Controls 2.2

Page {
    id: page
    x: 10
    y: 10
    width: 617
    height: 423

    Column {
        id: column
        width: 200
        anchors.bottom: parent.bottom
        anchors.top: parent.top

        Image {
            id: image
            x: 110
            y: 130
            width: 500
            height: 500
            fillMode: Image.PreserveAspectFit
            source: "bathymetry.bmp"
        }

        Text {
            id: picked
            text: qsTr("picked lon: xxx, lat:yyy,z:-zzz ")
            font.pixelSize: 12
        }

        Text {
            id: mouseMode
            text: qsTr("Mouse mode:")
            font.pixelSize: 12
        }
    }
}










/*##^## Designer {
    D{i:2;anchors_height:210;anchors_width:208;anchors_x:110;anchors_y:130}D{i:1;anchors_height:400;anchors_width:200}
}
 ##^##*/
