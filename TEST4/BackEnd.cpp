#include <getopt.h>
#include <QtDebug>
#include <QtDataVisualization/Q3DSurface>
#include <QtDataVisualization/QSurface3DSeries>
#include <QQuickItem>
#include <QCoreApplication>
#include <QValue3DAxis>
#include <QValue3DAxisFormatter>
#include <gmt/gmt.h>

#include "Backend.h"
#include "TopographicSeries.h"

Backend::Backend(QObject *parent) : QObject(parent)
{
    qDebug() << "*** Backend constructor!";
    m_gridFile = nullptr;
    m_topographicSeries = nullptr;

    QObject *object =
            g_rootWindow->findChild<QObject *>("surface3D");

    if (!object) {
        qCritical() << "Couldn't find \"surface3D\" object in GUI";
        return;
    }
    qDebug() << "Found \"surface3D\" in GUI";
    m_surface = (DeclarativeSurface *)object;

    // Process command-line arguments
    QString arg0 = QCoreApplication::arguments().at(0);
    qDebug() << "arg[0]: " << arg0;

}


void Backend::setGridFile(QUrl fileURL) {

    qDebug() << "*** setGridFile() - " << fileURL;

    if (m_gridFile) {
      free((void *)m_gridFile);
    }
    m_gridFile = strdup(fileURL.toLocalFile().toLatin1().data());
    
    if (m_topographicSeries) {
      // Need to remove series from surface before deleting the series
        m_surface->removeSeries(m_topographicSeries);
      
        delete m_topographicSeries;
    }
    m_topographicSeries = new TopographicSeries();

    
    void *gmtApi;
    GMT_GRID *gmtGrid =
      TopographicSeries::readGridFile(m_gridFile,
				      &gmtApi);
    if (!gmtGrid) {
        qCritical() << "Unable to open grid file " << fileURL;
        return;
    }
    qDebug() << "Opened " << fileURL;

    QObject *object;

    object = g_rootWindow->findChild<QObject *>("surface3DItem");
    if (!object) {
        qCritical() << "Couldn't find \"surface3DItem\" object in GUI";
        return;
    }
    qDebug() << "surface3DItem width: " << ((QQuickItem *)object)->width();
    qDebug() << "surface3DItem height: " << ((QQuickItem *)object)->height();
    qDebug() << "areaWidth: " << gmtGrid->header->wesn[1] - gmtGrid->header->wesn[0];
    qDebug() << "areaHeight: " << gmtGrid->header->wesn[3] - gmtGrid->header->wesn[2];

    // Load data into series
    m_topographicSeries->setTopography(gmtApi, gmtGrid);

    m_topographicSeries->setItemLabelFormat(QStringLiteral("@yLabel m"));
    qDebug() << "surface3D width: " << m_surface->width();
    qDebug() << "surface3D height: " << m_surface->height();

    qDebug() << "get series list";
    QQmlListProperty<QSurface3DSeries> seriesList;

    seriesList = m_surface->seriesList();
    qDebug() << "before clear - found " << m_surface->countSeriesFunc(&seriesList) << " series";
    m_surface->clearSeriesFunc(&seriesList);
    int n = m_surface->countSeriesFunc(&seriesList);
    qDebug() << "after clear - found " << n << " series";

    // Set series axes ranges
    double min = 0, max = 50;

    m_topographicSeries->longitRange(&min, &max);
    qDebug() << "X (longit) axis min: " << min << ", max: " << max;
    QValue3DAxis *axis = m_surface->axisX();
    qDebug() << "current X (longit) min: " << axis->min() << ", max: " << axis->max();
    axis->setRange(min, max);
    axis->setLabelFormat(QStringLiteral("%.0f"));
    axis->setTitle(gmtGrid->header->x_units);
    axis->setTitleVisible(true);

    m_topographicSeries->heightRange(&min, &max);
    qDebug() << "Y (height) axis min: " << min << ", max: " << max;
    axis = m_surface->axisY();
    qDebug() << "current Y (height) min: " << axis->min() << ", max: " << axis->max();
    axis->setRange(min, max);
    axis->setLabelFormat(QStringLiteral("%.0f"));
    axis->setTitle(gmtGrid->header->z_units); // note qt transposes z and y
    axis->setTitleVisible(true);

    m_topographicSeries->latitRange(&min, &max);
    qDebug() << "Z (latit) axis min: " << min << ", max: " << max;
    axis = m_surface->axisZ();
    qDebug() << "current Z (latit) min: " << axis->min() << ", max: " << axis->max();
    axis->setRange(min, max);
    axis->setLabelFormat(QStringLiteral("%.0f"));
    axis->setTitle(gmtGrid->header->y_units);  // note qt transposes z and y
    axis->setTitleVisible(true);

    qDebug() << "flat shading supported? " << m_topographicSeries->isFlatShadingSupported();
    m_topographicSeries->setFlatShadingEnabled(true);
    qDebug() << "flat shading enabled? " << m_topographicSeries->isFlatShadingEnabled();

    qDebug() << "addSeries()";
    m_surface->addSeries(m_topographicSeries);
    qDebug() << "returned from addSeries()";

    qDebug() << "after adding topo series, found " << m_surface->countSeriesFunc(&seriesList)
            << " series";


    object = g_rootWindow->findChild<QObject *>("mainWindow");
    if (!object) {
        qCritical("Could not find mainWindow");
        return;
    }
    g_rootWindow->setProperty("title", fileURL.toLocalFile());

}

void Backend::test() {
  qDebug() << "*** Backend::test()";
}


bool Backend::getOptions(int argc, char **argv) {

  const char *shortOptions = "I:";
  const option longOptions[] = {
				{"grid", required_argument, nullptr, 'I'},
				{nullptr, no_argument,nullptr, 0}
  };

  bool error = false;
  int opt;
  char *endptr;

  while ((opt = getopt_long(argc, argv, shortOptions, longOptions, nullptr)) != -1) {
    switch (opt) {
    case 'I':
      m_gridFile = strdup((const char *)&endptr);
      break;

    default:
      fprintf(stderr, "Unknown option: %s\n", opt);
      error = true;
    }
  }

  if (error) {
    printUsage();
    return false;
  }
  return true;
}


void Backend::printUsage() {
    fprintf(stderr, "usage goes here\n");
}
