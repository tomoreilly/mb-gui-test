/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the Qt Data Visualization module of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:GPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3 or (at your option) any later version
** approved by the KDE Free Qt Foundation. The licenses are as published by
** the Free Software Foundation and appearing in the file LICENSE.GPL3
** included in the packaging of this file. Please review the following
** information to ensure the GNU General Public License requirements will
** be met: https://www.gnu.org/licenses/gpl-3.0.html.
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include <gmt/gmt.h>
#include <sys/stat.h>
#include <unistd.h>
#include "topographicseries.h"

using namespace QtDataVisualization;

//! [0]
// Value used to encode height data as RGB value on PNG file
const float packingFactor = 11983.0f;
//! [0]

const float darkRedPos = 1.0f;
const float redPos = 0.8f;
const float yellowPos = 0.6f;
const float greenPos = 0.4f;
const float darkGreenPos = 0.2f;

TopographicSeries::TopographicSeries()
{
    setDrawMode(QSurface3DSeries::DrawSurface);
    setFlatShadingEnabled(true);
    toggleColorMap(true);
    // setBaseColor(Qt::white);
}

TopographicSeries::~TopographicSeries()
{
}

void TopographicSeries::setTopographyFile(const QString file, float width, float height)
{

    QImage heightMapImage(file);
    uchar *bits = heightMapImage.bits();
    int imageHeight = heightMapImage.height();
    int imageWidth = heightMapImage.width();
    int widthBits = imageWidth * 4;
    float stepX = width / float(imageWidth);
    float stepZ = height / float(imageHeight);

    QSurfaceDataArray *dataArray = new QSurfaceDataArray;
    dataArray->reserve(imageHeight);
    for (int i = 0; i < imageHeight; i++) {
        int p = i * widthBits;
        float z = height - float(i) * stepZ;
        QSurfaceDataRow *newRow = new QSurfaceDataRow(imageWidth);
        for (int j = 0; j < imageWidth; j++) {
            uchar aa = bits[p + 0];
            uchar rr = bits[p + 1];
            uchar gg = bits[p + 2];
            uint color = uint((gg << 16) + (rr << 8) + aa);
            float y = float(color) / packingFactor;
            (*newRow)[j].setPosition(QVector3D(float(j) * stepX, y, z));
            p = p + 4;
        }
        *dataArray << newRow;
    }

    dataProxy()->resetArray(dataArray);
//! [1]

    m_sampleCountX = float(imageWidth);
    m_sampleCountZ = float(imageHeight);
}

void TopographicSeries::setTopography(void *gmtApi, GMT_GRID *grid, float width, float height)
{
  int imageHeight = grid->header->n_rows;
  int imageWidth = grid->header->n_columns;
  int widthBits = imageWidth * 4;
  float stepX = width / float(imageWidth);
  float stepZ = height / float(imageHeight);

  QSurfaceDataArray *dataArray = new QSurfaceDataArray;

  dataArray->reserve(imageHeight);
  for (int row = 0; row < imageHeight; row++) {
      float z = (height - float(row) * stepZ) + grid->header->wesn[2];
      QSurfaceDataRow *newRow = new QSurfaceDataRow(imageWidth);
      for (int col = 0; col < imageWidth; col++) {
          //int index = row * imageWidth + col;
          int index = GMT_Get_Index(gmtApi, grid->header, row, col);
	  float y = grid->data[index];
          // (QSurface properly ignores NaN)
          float x = float(col) * stepX + grid->header->wesn[0];

          (*newRow)[col].setPosition(QVector3D(x, y, z));
      }
      *dataArray << newRow;
  }



    dataProxy()->resetArray(dataArray);
//! [1]

    m_sampleCountX = float(imageWidth);
    m_sampleCountZ = float(imageHeight);
}

GMT_GRID *TopographicSeries::readGridFile(const char *gridFile, void **api) {

  // Check for file existence and readability
  struct stat fileStatus;

  if (stat(gridFile, &fileStatus) != 0
      || (fileStatus.st_mode & S_IFMT) == S_IFDIR
      || fileStatus.st_size <= 0) {
      qCritical() << "Can not read \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile: %s\n", gridFile);
  // Create GMT API
  *api =
      GMT_Create_Session("Topography::loadGrid()", 2U, 0U, nullptr);

  if (!*api) {
      qCritical() << "Could not get GMT API for \"" << gridFile << "\"";
      return nullptr;
    }

  fprintf(stderr, "gridFile now: %s\n", gridFile);

  GMT_GRID *grid = nullptr;
  // Try to read header and grid
  for (int nTry = 0; nTry < 100; nTry++) {
      grid = (struct GMT_GRID *)GMT_Read_Data(*api, GMT_IS_GRID, GMT_IS_FILE, GMT_IS_SURFACE,
                                                   GMT_GRID_ALL, nullptr, gridFile, nullptr);
      if (grid) break;
      usleep(1000);
    }

  if (!grid) {
      qCritical() << "Unable to read GMT grid from \"" << gridFile << "\"";
      return nullptr;
    }
  return grid;
}

void TopographicSeries::toggleColorMap(bool on) {

  if (on) {
  float ratio = 1.f;

  QLinearGradient gr;
  gr.setColorAt(0.0f, Qt::black);
  gr.setColorAt(darkGreenPos * ratio, Qt::darkGreen);
  gr.setColorAt(greenPos * ratio, Qt::green);
  gr.setColorAt(yellowPos * ratio, Qt::yellow);
  gr.setColorAt(redPos * ratio, Qt::red);
  gr.setColorAt(darkRedPos * ratio, Qt::darkRed);

  setBaseGradient(gr);
  setColorStyle(Q3DTheme::ColorStyleRangeGradient);
}
  else {
     setBaseColor(Qt::white);
    }
}
