#version 330
// layout(location = 0) in vec3 position;
// layout(location = 1) in vec3 normal;
// layout(location = 2) in vec4 color;
attribute vec3 a_Position;
attribute vec3 a_Normal;
attribute vec4 a_Color;

out vec4 vColor;

void main()
{
  gl_Position = vec4(a_Position, 1.0);
  vColor = a_Color;
}
