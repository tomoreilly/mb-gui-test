#ifndef GMTGRIDSURFACE_H
#define GMTGRIDSURFACE_H

#include "gmt.h"
#include "Surface.h"

/** 
Build a GMT grid surface from contents of a GMT grid file
 ** */
class GmtGridSurface : public Surface {

 public:

  GmtGridSurface();

  ~GmtGridSurface();
  
  /// Build surface from GMT grid file
  bool build(const char *gridFile);
  
  /// Read grid from GMT file; return pointer to GMT_GRID if
  /// successful, else return nullptr
  static GMT_GRID *readGridFile(const char *filename, void **gmtApi);  

 protected:
  

};

#endif // GMTGRIDSURFACE_H
