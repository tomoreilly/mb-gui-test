#ifndef PARABOLICSURFACE_H
#define PARABOLICSURFACE_H

#include <QVector>
#include <QVector3D>
#include "Surface.h"

/* ***
 ParabolicSurface builds parabolic surface represented as vertices, normals,
and triangle-drawing indices.
*/

class ParabolicSurface : public Surface {

 public:

  ParabolicSurface();

  /// Generate vertices, normals, indices
  void build(int verticesPerSide);
  
 protected:

  
};

#endif // PARABOLICSURFACE_H
