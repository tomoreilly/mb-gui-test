#ifndef PARABOLAWINDOW_H
#define PARABOLAWINDOW_H

#include <QOpenGLWindow>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLVertexArrayObject>
#include <QMatrix4x4>

class QOpenGLShaderProgram;

class ParabolaWindow : public QOpenGLWindow,
               protected QOpenGLFunctions
{
  Q_OBJECT

// OpenGL Events
public:
  ParabolaWindow();
  void initializeGL();
  void resizeGL(int width, int height);
  void paintGL();

  protected slots:
  void teardownGL();

private slots:
  /// Called when window size changes
  //  void resizeEvent(QResizeEvent *event);

 protected:

  /// Indicates if OpenGL functions are initialized
  bool m_openGLInitialized = false;

  /// Set parabolic function vertices
  void setData();

  /// Initialize matrices
  void initializeMatrices();

  /// Shaders
  QOpenGLShaderProgram *m_program;

  /// VAO
  QOpenGLVertexArrayObject m_vao;

  /// VBO
  QOpenGLBuffer m_vbo;

  /// IBO
  QOpenGLBuffer m_ibo;

  /// Size of the viewport
  QSize m_viewportSize;

  /// Size characteristics of parabola height map
  static const int VerticesPerSide = 34;   // was 32
  /// x, y, z
  static const int PositionTupleSize = 3;
  /// x, y, z
  static const int NormalTupleSize = 3;
  /// R, G, B, Alpha
  static const int ColorTupleSize = 4;


  /// Model matrix; moves model from object space to world space
  QMatrix4x4 m_modelMatrix;

  /// Projection matrix; projects scene onto viewport
  QMatrix4x4 m_projectionMatrix;

  /// View matrix; "camera", transforms world space to eye space
  QMatrix4x4 m_viewMatrix;

  /// Light model matrix
  QMatrix4x4 m_lightModelMatrix;

  /// Combined matrix
  QMatrix4x4 m_mvpMatrix;

  /// Current rotation matrix
  QMatrix4x4 m_rotationMatrix;

  /// Accumulated rotation matrix
  QMatrix4x4 m_accumRotationMatrix;

  /// Temporary matrix
  QMatrix4x4 m_tempMatrix;

  /// Current light position in world space (after transformation
  /// via model matrix)
  QVector4D m_worldLightPos;

  /// Light position in eye space (after transformation via model-view
  /// matrix)
  QVector4D m_eyeLightPos;

  /// Light centered at model space origin
  QVector4D m_modelLightPos;



  /// Attributes for shaders
  static const char *PositionAttrName;
  int m_positionAttr = 0;
  static const char *NormalAttrName;
  int m_normalAttr = 0;
  static const char *ColorAttrName;
  int m_colorAttr = 0;

  /// Shader matrix "uniform" variables
  static const char *LightPositionName;
  static const char *MvpMatrixName;
  static const char *MvMatrixName;

  static const int Stride =
      sizeof(float) * (PositionTupleSize + NormalTupleSize + ColorTupleSize);

  static const float MinPosition;
  static const float PositionRange;

  GLfloat *m_mapVertices;
  GLuint *m_mapIndexData;

  /// Number of elements in m_mapVertices array
  int m_verticesArraySize;

  /// Number of elements in m_mapIndexData array
  int m_indexArraySize;

  bool m_testData;


};

#endif // PARABOLAWINDOW_H
