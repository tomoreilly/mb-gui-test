#include "GmtGridSurface.h"


GmtGridSurface::GmtGridSurface() {
}



bool GmtGridSurface::build(const char *gridFile) {

  void *gmtApi = nullptr;
  GMT_GRID *gmtGrid = readGridFile(gridFile, &gmtApi);
  if (!gmtGrid) {
    fprintf(stderr, "Couldn't open grid file \"%s\"\n", gridFile);
    return false;
  }

  setData(gmtApi, gmtGrid);
  
  return true;
}


GMT_GRID *TopographicSeries::readGridFile(const char *gridFile,
					  void **api) {
    fprintf(stderr, "readGridFile(): gridFile: %s\n", gridFile);
    // Check for file existence and readability
    struct stat fileStatus;

    if (stat(gridFile, &fileStatus) != 0
            || (fileStatus.st_mode & S_IFMT) == S_IFDIR
            || fileStatus.st_size <= 0) {
        qCritical() << "Can not read \"" << gridFile << "\"";
        return nullptr;
    }

    fprintf(stderr, "readGridFile(): create session\n");
    // Create GMT API
    *api =
            GMT_Create_Session("Topography::loadGrid()", 2U, 0U, nullptr);

    if (!*api) {
        qCritical() << "Could not get GMT API for \"" << gridFile << "\"";
        return nullptr;
    }

    fprintf(stderr, "gridFile now: %s\n", gridFile);

    GMT_GRID *grid = nullptr;
    // Try to read header and grid
    for (int nTry = 0; nTry < 100; nTry++) {
        grid = (struct GMT_GRID *)GMT_Read_Data(*api, GMT_IS_GRID, GMT_IS_FILE, GMT_IS_SURFACE,
                                                GMT_GRID_ALL, nullptr, gridFile, nullptr);
        if (grid) break;
        usleep(1000);
    }

    if (!grid) {
        qCritical() << "Unable to read GMT grid from \"" << gridFile << "\"";
        return nullptr;
    }
    return grid;
}


void GmtGridSurface::setData(void *gmtApi, GMT_GRID *gmtGrid) {

  struct GMT_GRID_HEADER *header = gmtGrid->header;

  m_nGridVertices = header->n_columns * header->n_rows;

  float zRange = 0.;
  // Get zRange and zMin for coloring algorithm; clip at z=0
  if (header->z_max > 0) {
    zRange = (0 - header->z_min);    
  }
  else {
    zRange = (header->z_max - header->z_min);
  }
  float zMin = header->z_min;

  float zPrev = 0.f;
  int vIndex = 0;   // Index into m_gridVertices array
  // Populate vertices with GMT_GRID data
  for (uint row = 0; row < header->n_rows; row++) {
      for (uint col = 0; col < header->n_columns; col++) {
          uint dataInd =
	    GMT_Get_Index(gmtApi, gmtGrid->header, row, col);

          float z = gmtGrid->data[dataInd];
          if (isnanf(z)) {
              z = zPrev;  // Extrapolate previous value
            }

	  m_positions.append(QVector3D((float )gmtGrid->x[col],
				       (float )gmtGrid->y[row],
				       z));
          zPrev = z;

          // Estimate normal vector at this point, based on slope
          if (col > 0 && row > 0) {
              // Get z at (prev-x, y)
              uint prevInd = GMT_Get_Index(gmtApi, gmtGrid->header, row, col-1);
              float slopeX = (z - gmtGrid->data[prevInd]) / (float )(gmtGrid->x[col] - gmtGrid->x[col-1]);

              // Get z at (x, prev-y)
              prevInd = (row - 1) * header->n_rows + col;
              prevInd = GMT_Get_Index(gmtApi, gmtGrid->header, row-1, col);
              float slopeY = (z - gmtGrid->data[prevInd]) / (float )(gmtGrid->y[row] - gmtGrid->y[row-1]);

              // Normal vector is cross-product of slopeX ad slopeY
              const float planeVecX[] = {1.f, 0., slopeX};
              const float planeVecY[] = {0., 1.f, slopeY};

              float normVec[] =
              {(planeVecX[1] * planeVecY[2] - planeVecX[2] * planeVecY[1]),
               (planeVecX[2] * planeVecY[0] - planeVecX[0] * planeVecY[2]),
               (planeVecX[0] * planeVecY[1] - planeVecX[1] * planeVecY[0])};

              // Normalize the normal vector
              float length = sqrt(normVec[0]*normVec[0] +
                   normVec[1]*normVec[1] + normVec[2]*normVec[2]);

	       m_normals.append(Q3DVector(normVec[0] / length,
					  normVec[1] / length,
					  normVec[2] / length));
	  }
          else {  // Point along x=0 or y=0 edge
              // Assume some slope for points at edge of grid
	      n_normals.append(Q3DVector(0., 0., 0.));
	  }

	  /* ***
          float red = 1.f, green = 0.f, blue = 0.f;
          // Assign color based on z
          setColor(z, zMin, zRange, &red, &green, &blue);  // Return white for now!
          m_gridVertices[vIndex++] = red;  // red
          m_gridVertices[vIndex++] = green;  // green
          m_gridVertices[vIndex++] = blue;  // blue
          m_gridVertices[vIndex++] = 1.f;  // alpha
          // fprintf(stderr, "red: %.3f, green: %.1f, blue: %.1f :::", red, green, blue);
	  *** */
        }
    }

  qDebug() << "m_nGridVertices: " << m_nGridVertices << ", vIndex: " << vIndex;

  int gIndex = 0;
  for (int row = 0; row < header->n_rows - 1; row++) {
      for (int col = 0; col < header->n_columns - 1; col++) {

          // First triangle
 	  m_indices.append(vertexIndex(col, row, header->n_columns));
	  m_indices.append(vertexIndex(col+1, row, header->n_columns));
	  m_indices.append(vertexIndex(col+1, row+1, header->n_columns));
          // Second triangle
          indices[gIndex++] = vertexIndex(col, row, header->n_columns);
          indices[gIndex++] = vertexIndex(col+1, row+1, header->n_columns);
          indices[gIndex++] = vertexIndex(col, row+1, header->n_columns);
	  m_indices.append(vertexIndex(col, row, header->n_columns));
	  m_indices.append(vertexIndex(col+1, row+1, header->n_columns));
	  m_indices.append(vertexIndex(col, row+1, header->n_columns));

        }
    }

  qDebug("Done with setData()");
}
