#include <math.h>
#include <GL/gl.h>
#include "ParabolicSurface.h"

ParabolicSurface::ParabolicSurface() {
}


void ParabolicSurface::build(int verticesPerSide) {

  initialize();
  
  const float MinPosition = -2.5;     // original -5.
  const float PositionRange = 5.;   // original 10.
 
  const int xLength = verticesPerSide;
  const int yLength = verticesPerSide;

  // Build data for vertex buffer
  for (int y = 0; y < yLength; y++) {
    for (int x = 0; x < xLength; x++) {
      float xRatio = x / (float)(xLength - 1.);

      // Build height map from top down for counter-clockwise
      // triangles
      float yRatio = 1. - (y / (float)(yLength - 1.));
      float xPos = MinPosition + (xRatio * PositionRange);
      float yPos = MinPosition + (yRatio * PositionRange);

      // Divide z by 10 so parabola is not too steep
      float zPos = ((xPos * xPos) + (yPos * yPos)) / 10.f;

      m_positions.append(QVector3D(xPos, yPos, zPos));
			  
      if (xPos < m_xMin) { m_xMin = xPos;}
      if (xPos > m_xMax) { m_xMax = xPos; }
      if (yPos < m_yMin) { m_yMin = yPos;}
      if (yPos > m_yMax) { m_yMax = yPos; }
      if (zPos < m_zMin) { m_zMin = zPos;}
      if (zPos > m_zMax) { m_zMax = zPos; }

      // Cheap normal using derivative of parabolic function
      // slope for x will be 2x, for y will be 2y.
      float xSlope = 2 * xPos / 10.f;
      float ySlope = 2 * yPos / 10.f;

      // Compute normal as cross-product of slopes
      const float planeVecX[] = {1.f, 0., xSlope};
      const float planeVecY[] = {0., 1.f, ySlope};

      float normVec[] =
	{(planeVecX[1] * planeVecY[2] - planeVecX[2] * planeVecY[1]),
	 (planeVecX[2] * planeVecY[0] - planeVecX[0] * planeVecY[2]),
	 (planeVecX[0] * planeVecY[1] - planeVecX[1] * planeVecY[0])};

      // Normalize the normal vector
      float length = sqrt(normVec[0]*normVec[0] +
			  normVec[1]*normVec[1] + normVec[2]*normVec[2]);

      // Normal vertex
      m_normals.append(QVector3D(normVec[0]/length,
			      normVec[1]/length,
			      normVec[2]/length));
    }
  }
				    
  // Build index data
  for (int y = 0; y < yLength - 1; y++) {
    if (y > 0) {
      // Degenerate begin; repeat first vertex
      m_indices.append((GLuint )(y * yLength));
    }
    for (int x = 0; x < xLength; x++) {
      m_indices.append((GLuint )((y * yLength) + x));
      m_indices.append((GLuint )(((y + 1) * yLength) + x));
    }

    if (y < yLength - 2) {
      // Degenerate end; repeat last vertex
      m_indices.append((GLuint )(((y + 1) * yLength) + (xLength - 1)));
    }
  }
  return;
}



