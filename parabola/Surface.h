#ifndef SURFACE_H
#define SURFACE_H

#include <QVector>
#include <QVector3D>

/* ***
 Surface represents a surface as vertices, normals,
 and triangle-drawing indices.
*/

class Surface {

 public:

  Surface();

  /// Return vertex positions
  QVector<QVector3D> vertices() { return m_positions; }

  /// Return normals to each triangle
  QVector<QVector3D> normals() { return m_normals; }

  /// Return triangle-drawing indices
  QVector<unsigned int> indices() { return m_indices; }

  /// Return "center" of surface in world coordinates
  void center(float *x, float *y, float *z);

  /// Return x, y, and z spans of surface
  void spans(float *xSpan, float *ySpan, float *zSpan);

  /// Return span of x values
  float xSpan() {
    return (m_xMax - m_xMin);
  }

  /// Return span of y values  
  float ySpan() {
    return (m_yMax - m_yMin);
  }

  /// Return span of z values  
  float zSpan() {
    return (m_zMax - m_zMin);
  }
  
  
  /// Generate vertices, normals, indices
  void build(int verticesPerSide);
  
 protected:

  void initialize();
  
  /// Surface points
  QVector<QVector3D> m_positions;

  /// Normals to surface points
  QVector<QVector3D> m_normals;

  /// Triangle drawing indices
  QVector<unsigned int> m_indices;


  // Surface extents
  float m_xMin;
  float m_xMax;
  float m_yMin;
  float m_yMax;
  float m_zMin;
  float m_zMax;
  

};

#endif // SURFACE_H
