#include "ParabolaWindow.h"
#include <math.h>
#include <QDebug>
#include <QString>
#include <QOpenGLShaderProgram>
#include <QVector3D>
#include <QResizeEvent>

const float ParabolaWindow::MinPosition = -5.;     // original -5.
const float ParabolaWindow::PositionRange = 10.;   // original 10.

const char * ParabolaWindow::PositionAttrName = "a_Position";
const char * ParabolaWindow::NormalAttrName = "a_Normal";
const char * ParabolaWindow::ColorAttrName = "a_Color";
const char * ParabolaWindow::LightPositionName = "u_LightPos";
const char * ParabolaWindow::MvpMatrixName = "u_MVPMatrix";
const char * ParabolaWindow::MvMatrixName = "u_MVMatrix";

float testVertices[] = {
  0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f, // top right
  0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 1.0f, 0.0f, 1.0f, // bottom right
  -0.5f, -0.5f, 0.0f, 0.f, 0.f, 0.f, 0.0f, 0.0f, 1.0f, 1.0f, // bottom left
  -0.5f,  0.5f, 0.0f, 0.f, 0.f, 0.f, 1.0f, 0.0f, 0.0f, 1.0f   // top left
};

GLuint testIndices[] = {  // note that we start from 0!
                          0, 1, 3,   // first triangle
                          1, 2, 3    // second triangle
                       };

ParabolaWindow::ParabolaWindow() :
  m_mapVertices(nullptr), m_mapIndexData(nullptr),
  m_verticesArraySize(0), m_indexArraySize(0),
  m_ibo(QOpenGLBuffer::IndexBuffer), m_testData(false),
  m_modelLightPos(0.f, 0.f, 0.f, 1.0f)
{

}


void ParabolaWindow::initializeGL()
{
  // Initialize OpenGL Backend
  initializeOpenGLFunctions();
  m_openGLInitialized = true;

  int maxAttributes;
  glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &maxAttributes);
  qDebug() << "Maximum " << maxAttributes << " vertex attributes allowed by GPU";

  QSize wSize = size();
  resizeGL(wSize.width(), wSize.height());

  connect(context(), SIGNAL(aboutToBeDestroyed()), this,
          SLOT(teardownGL()), Qt::DirectConnection);

  // Load parabola data into vertices and index
  setData();
  
  // Initialze matrices
  initializeMatrices();

  // Enable depth-testing
  glEnable(GL_DEPTH_TEST);

  glClearColor(0.f, 0.f, 0.f, 1.0f);

  // Application-specific stuff
  {

    const char *vertexShader = ":/shaders/test.vert";
    const char *fragmentShader = ":/shaders/test.frag";

    // Create shader program, add, link and bind shaders
    m_program = new QOpenGLShaderProgram();
    fprintf(stderr, "m_program: %p\n", m_program);
    if (!m_program->addShaderFromSourceFile(QOpenGLShader::Vertex,
                                            vertexShader)) {
        // Error
        QString msg = m_program->log();
        qCritical() << "Error adding vertex shader:\n" + msg.toLatin1();
        return;
      }
    qDebug("added vertex shader");

    if (!m_program->addShaderFromSourceFile(QOpenGLShader::Fragment,
                                            fragmentShader)) {
        // Error
        QString msg = m_program->log();
        qCritical() << "Error adding fragment shader:\n" << msg.toLatin1();
        return;
      }
    qDebug("added fragment shader");

    if (!m_program->link()) {
        // Error
        fprintf(stderr, "shader linking failed\n");
        return;
      }
    qDebug("shaders linked ok");

    m_program->bind();

    if (!m_vao.create()) {
        // Error
        fprintf(stderr, "VAO create() failed\n");
        return;
      }
    m_vao.bind();

    m_vbo.create();
    m_vbo.bind();
    m_vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_ibo.create();
    m_ibo.bind();
    m_ibo.setUsagePattern(QOpenGLBuffer::StaticDraw);

    if (m_testData) {
        m_vbo.allocate(testVertices, sizeof(testVertices));
        m_ibo.allocate(testIndices, sizeof(testIndices));
      }
    else {
        m_vbo.allocate(m_mapVertices, m_verticesArraySize * sizeof(GL_FLOAT));
        m_ibo.allocate(m_mapIndexData, m_indexArraySize * sizeof(GL_UNSIGNED_INT));
      }

    // Configure attributes for shader
    m_positionAttr = glGetAttribLocation(m_program->programId(), PositionAttrName);
    m_normalAttr = glGetAttribLocation(m_program->programId(), NormalAttrName);
    m_colorAttr = glGetAttribLocation(m_program->programId(), ColorAttrName);
    fprintf(stderr, "positionAttr: %d, normalAttr: %d, colorAttr: %d\n",
            m_positionAttr, m_normalAttr, m_colorAttr);

    m_program->setAttributeBuffer(m_positionAttr, GL_FLOAT, 0,
                                  PositionTupleSize, Stride);

    m_program->setAttributeBuffer(m_normalAttr, GL_FLOAT,
                                  PositionTupleSize * sizeof(GL_FLOAT),
                                  NormalTupleSize, Stride);

    m_program->setAttributeBuffer(m_colorAttr, GL_FLOAT,
                                  (PositionTupleSize + NormalTupleSize) *
                                  sizeof(GL_FLOAT),
                                  ColorTupleSize, Stride);

    m_program->enableAttributeArray(m_positionAttr);
    m_program->enableAttributeArray(m_normalAttr);
    m_program->enableAttributeArray(m_colorAttr);

    // Unbind
    // do NOT unbind the IBO while a VAO is active as the bound element buffer object IS stored in the VAO; keep the IBO bound.
    // (https://learnopengl.com/code_viewer_gh.php?code=src/1.getting_started/2.2.hello_triangle_indexed/hello_triangle_indexed.cpp)
    // m_ibo.release();
    m_vbo.release();
    m_vao.release();
    m_program->release();
  }
}

void ParabolaWindow::initializeMatrices() {

  qDebug("initializeMatrices()");
  return;
}

void ParabolaWindow::resizeGL(int width, int height)
{
  qDebug("ParabolaWindow::resizeGL()");
  if (!m_openGLInitialized) {
      qDebug("resizeGL() - OpenGL not yet initialized");
      return;
    }
  qDebug("ParabolaWindow::resizeGL - set viewport size");
  // Set OpenGL viewport to be the same dimension as window
  glViewport(0, 0, width, height);

  // Create a new perspective projection matrix. The height will stay the
  // same while the width will vary as per aspect ratio.
  float ratio = (float) width / height;
  float left = -ratio;
  float right = ratio;
  float bottom = -1.0f;
  float top = 1.0f;
  float near = 1.0f;
  float far = 1000.0f;

  m_projectionMatrix.frustum(left, right, bottom, top, near, far);
}

void ParabolaWindow::setData() {

  const int floatsPerVertex =
     PositionTupleSize + NormalTupleSize + ColorTupleSize;

  const int xLength = VerticesPerSide;
  const int yLength = VerticesPerSide;

  m_verticesArraySize = xLength * yLength * floatsPerVertex;
  m_mapVertices = (GLfloat *)malloc(m_verticesArraySize * sizeof(GLfloat));

  int offset = 0;

  float minX = std::numeric_limits<float>::max();
  float maxX = -std::numeric_limits<float>::max();
  float minY = std::numeric_limits<float>::max();
  float maxY = -std::numeric_limits<float>::max();
  float minZ = std::numeric_limits<float>::max();
  float maxZ = -std::numeric_limits<float>::max();

  // Build data for vertex buffer
  for (int y = 0; y < yLength; y++) {
      for (int x = 0; x < xLength; x++) {
          float xRatio = x / (float)(xLength - 1.);

          // Build height map from top down for counter-clockwise
          // triangles
          float yRatio = 1. - (y / (float)(yLength - 1.));
          float xPos = MinPosition + (xRatio * PositionRange);
          float yPos = MinPosition + (yRatio * PositionRange);
          // Position
          m_mapVertices[offset++] = xPos;
          m_mapVertices[offset++] = yPos;
          // Divide z by 10 so parabola is not too steep
          float zPos = ((xPos * xPos) + (yPos * yPos)) / 10.f;
          m_mapVertices[offset++] = zPos;

          if (xPos < minX) { minX = xPos;}
          if (xPos > maxX) { maxX = xPos; }
          if (yPos < minY) { minY = yPos;}
          if (yPos > maxY) { maxY = yPos; }
          if (zPos < minZ) { minZ = zPos;}
          if (zPos > maxZ) { maxZ = zPos; }

          // Cheap normal using derivative of parabolic function
          // slope for x will be 2x, for y will be 2y.
          float xSlope = 2 * xPos / 10.f;
          float ySlope = 2 * yPos / 10.f;

          // Compute normal as cross-product of slopes
          const float planeVecX[] = {1.f, 0., xSlope};
          const float planeVecY[] = {0., 1.f, ySlope};

          float normVec[] =
          {(planeVecX[1] * planeVecY[2] - planeVecX[2] * planeVecY[1]),
           (planeVecX[2] * planeVecY[0] - planeVecX[0] * planeVecY[2]),
           (planeVecX[0] * planeVecY[1] - planeVecX[1] * planeVecY[0])};

          // Normalize the normal vector
          float length = sqrt(normVec[0]*normVec[0] +
              normVec[1]*normVec[1] + normVec[2]*normVec[2]);

          // Normal vertex
          m_mapVertices[offset++] = normVec[0] / length;
          m_mapVertices[offset++] = normVec[1] / length;
          m_mapVertices[offset++] = normVec[2] / length;

          // Some fancy colors
          m_mapVertices[offset++] = xRatio;
          m_mapVertices[offset++] = yRatio;
          m_mapVertices[offset++] = 0.5f;
          m_mapVertices[offset++] = 1.0f;
        }
    }

  // Build index data
  int nStrips = yLength - 1;
  int nDegenerates = 2 * (nStrips - 1);
  int verticesPerStrip = 2 * xLength;

  m_indexArraySize = verticesPerStrip * nStrips + nDegenerates;
  m_mapIndexData = (GLuint *)malloc(m_indexArraySize * sizeof(GLuint));

  offset = 0;
  for (int y = 0; y < yLength - 1; y++) {
      if (y > 0) {
          // Degenerate begin; repeat first vertex
          m_mapIndexData[offset++] = (GLuint )(y * yLength);
        }
      for (int x = 0; x < xLength; x++) {
          m_mapIndexData[offset++] = (GLuint )((y * yLength) + x);
          m_mapIndexData[offset++] = (GLuint )(((y + 1) * yLength) + x);
        }

      if (y < yLength - 2) {
          // Degenerate end; repeat last vertex
          m_mapIndexData[offset++] = (GLuint )(((y + 1) * yLength) + (xLength - 1));
        }
    }
  qDebug("done with setData()");
}

void ParabolaWindow::paintGL() {

  glClear(GL_COLOR_BUFFER_BIT);
  if (!m_program->bind()) {
      QString msg = m_program->log();
      qCritical() << "Couldn't bind program:\n" << msg.toLatin1();
      return;
    }

  GLint model = glGetUniformLocation(m_program->programId(), "model");
  if (model == -1) {
      qCritical() << "variable \"" << model << "\" not found in shader";
      return;
    }

  GLint view = glGetUniformLocation(m_program->programId(), "view");
  if (view == -1) {
      qCritical() << "variable \"" << view << "\" not found in shader";
      return;
    }

  GLint projection = glGetUniformLocation(m_program->programId(), "projection");
  if (projection == -1) {
      qCritical() << "variable \"" << projection << "\" not found in shader";
      return;
    }

  GLint lightPos = glGetUniformLocation(m_program->programId(), LightPositionName);
  if (lightPos == -1) {
      qCritical() << "variable " << LightPositionName << " not found in shader";
      return;
    }

  if (m_testData) {
      // Set transforms for test data shape
      qDebug("Set transform to identity matrix");
      m_modelMatrix.setToIdentity();

      m_modelMatrix.rotate(0.f, 1.0f, 0.f, 0.f);
      m_modelMatrix.rotate(0.f, 0.f, 1.0f, 0.f);
      m_viewMatrix.setToIdentity();

      // Move back along z-axis
      m_viewMatrix.translate(0.f, 0.f, -10.0f);

      m_projectionMatrix.setToIdentity();
      m_projectionMatrix.perspective(45.f, 0.8, 0.1, 1000.f);

      qDebug("send modelMatrix to shader");
      glUniformMatrix4fv(model, 1, false, m_modelMatrix.constData());

      qDebug("send viewMatrix to shader");
      glUniformMatrix4fv(view, 1, false, m_viewMatrix.constData());

      qDebug("send projectionMatrix to shader");
      glUniformMatrix4fv(projection, 1, false, m_projectionMatrix.constData());

      // Set lighting
      m_lightModelMatrix.setToIdentity();
      m_lightModelMatrix.translate(0.f, 1.f, 5.0f);

      // m_modelLightPos(0.f, 0.f, 0.f, 1.0f);
      m_modelLightPos.setZ(1.9f);
      m_worldLightPos = m_lightModelMatrix * m_modelLightPos;
      m_eyeLightPos = m_viewMatrix * m_worldLightPos;

      qDebug("send eye-light-position to shader");
      glUniform3f(lightPos, m_worldLightPos[0], m_worldLightPos[1], m_worldLightPos[2]);

    }
  else {
      // Set transforms for parabola surface
      qDebug("Set transform to identity matrix");
      m_modelMatrix.setToIdentity();

      m_modelMatrix.rotate(-55.f, 1.0f, 0.f, 0.f);
      m_modelMatrix.rotate(-45.f, 0.f, 1.0f, 0.f);
      m_viewMatrix.setToIdentity();
      // Move back along z-axis
      m_viewMatrix.translate(0.f, 0.f, -30.0f);

      m_projectionMatrix.setToIdentity();
      m_projectionMatrix.perspective(45.f, 0.8, 0.1, 1000.f);

      qDebug("send modelMatrix to shader");
      glUniformMatrix4fv(model, 1, false, m_modelMatrix.constData());

      qDebug("send viewMatrix to shader");
      glUniformMatrix4fv(view, 1, false, m_viewMatrix.constData());

      qDebug("send projectionMatrix to shader");
      glUniformMatrix4fv(projection, 1, false, m_projectionMatrix.constData());

      // Set lighting
      m_lightModelMatrix.setToIdentity();
      m_lightModelMatrix.translate(0.f, 5.5f, 15.0f);

      // m_modelLightPos(0.f, 0.f, 0.f, 1.0f);
      m_modelLightPos.setZ(1.9f);
      m_worldLightPos = m_lightModelMatrix * m_modelLightPos;
      m_eyeLightPos = m_viewMatrix * m_worldLightPos;

      qDebug("send eye-light-position to shader");
      glUniform3f(lightPos, m_worldLightPos[0], m_worldLightPos[1], m_worldLightPos[2]);

    }

  m_vao.bind();
  fprintf(stderr, "draw elements\n");
  if (m_testData) {
      glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    }
  else {
      glDrawElements(GL_TRIANGLE_STRIP, m_indexArraySize, GL_UNSIGNED_INT, 0);
    }

  m_vao.release();

  m_program->release();
  glFlush();
  return;

}


void ParabolaWindow::teardownGL()
{
  m_vbo.destroy();
  m_vao.destroy();
  m_ibo.destroy();
  
  delete m_program;
  
  fprintf(stderr, "HeightMapRenderer dstr\n");
  if (m_mapVertices) {
      free(m_mapVertices);
      m_mapVertices = nullptr;
    }
  if (m_mapIndexData) {
      free(m_mapIndexData);
      m_mapIndexData = nullptr;
    }
}

