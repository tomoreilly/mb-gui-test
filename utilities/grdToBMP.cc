#include <stdio.h>
#include <math.h>
#include <gmt/gmt.h>

const float NoDataValue = -9999999.f;

int main(int argc, char **argv) {

  if (argc != 2) {
    fprintf(stderr, "usage: %s  gridFile\n", argv[0]);
    return -1;
  }
  char *gridFile = argv[1];

  // Open API
  void *api = GMT_Create_Session("readGrid", 2U, 0U, nullptr);
  if (!api) {
    fprintf(stderr, "Couldn't create GMT API\n");
    return -1;
  }
    
  // Read data
  GMT_GRID *grid =
    (GMT_GRID *)GMT_Read_Data(api, GMT_IS_GRID,
			      GMT_IS_FILE,
			      GMT_IS_SURFACE,
			      GMT_GRID_ALL, nullptr,
			      gridFile, nullptr);
  if (!grid) {
    fprintf(stderr, "Couldn't read GMT grid from %s\n",
	    gridFile);
    return -1;
  }
    
  fprintf(stderr, "n_columns: %d, n_rows: %d\n",
	  grid->header->n_columns, grid->header->n_rows);

  fprintf(stderr, "pad[0]: %d, pad[1]: %d, pad[2]: %d, pad[3]: %d\n",
	  grid->header->pad[0], grid->header->pad[1],
	  grid->header->pad[2], grid->header->pad[3]);

  for (int row = 0; row < grid->header->n_rows; row++) {
    for (int col = 0; col < grid->header->n_columns; col++) {
      int ind = row*grid->header->n_columns + col;
      fprintf(stderr, "col %d: %.5f row %d: %.5f z: %.5f\n",
	      col, grid->x[col], row, grid->y[row],
	      grid->data[ind]);
      if (isnanf(grid->data[ind])) {
	fprintf(stderr, "(z is nanf\n");
      }

    }
  }
  fprintf(stderr, "\n");

  return 0;
}
