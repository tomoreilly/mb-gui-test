#include <stdio.h>
#include <vtkLookupTable.h>
#include "Utilities.h"

using namespace mb_system;

int main(int argc, char **argv) {

  int colorScheme = 2;
  
  vtkSmartPointer<vtkLookupTable> lut =
    vtkSmartPointer<vtkLookupTable>::New();
     
  makeLookupTable(colorScheme, lut);

  return 0;
}

