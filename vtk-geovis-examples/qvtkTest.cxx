// These first three lines address
// issue described at
// https://stackoverflow.com/questions/18642155/no-override-found-for-vtkpolydatamapper
#include "vtkAutoInit.h" 
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

#include <unistd.h>

#include "GL/freeglut.h"
#include "GL/gl.h"
#include "GL/glext.h"

#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkCubeAxesActor.h>
#include <vtkCamera.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkActor2D.h>
#include <vtkProperty.h>
#include <vtkStringArray.h>
#include <vtkPolyDataMapper.h>
#include <vtkImageMapper.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkElevationFilter.h>
#include <vtkLookupTable.h>
#include <vtkColorSeries.h>
#include <vtkNamedColors.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <vtkDataObject.h>
#include <vtkGradientFilter.h>
#include <vtkInteractorStyleTerrain.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTextProperty.h>
#include <vtkInteractorStyleTerrain.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkPointPicker.h>
#include <vtkCallbackCommand.h>

// Cause crash???
#include <vtkGenericRenderWindowInteractor.h>
#include <vtkOpenGLRenderWindow.h>
// #include <vtkGenericOpenGLRenderWindow.h>
#include "Utilities.h"
#include "QVtkRenderer.h"

// Use of vtkGenericOpenGLRenderWindow with
// vtkGenericRenderWindowInteractor
// leads to seg fault

using namespace mb_system;


void isCurrentCallback(vtkObject *, unsigned long eid, void *clientData,
                       void *callData) {

  std::cout << "isCurrentCallback()!" << std::endl;
  
}




// Define interaction style
class MouseInteractorStyle : public vtkInteractorStyleTrackballCamera
{
public:
  static MouseInteractorStyle* New();
  vtkTypeMacro(MouseInteractorStyle, vtkInteractorStyleTrackballCamera);

  virtual void OnLeftButtonDown() override
  {
    std::cout << "Picking pixel: " << this->Interactor->GetEventPosition()[0]
              << " " << this->Interactor->GetEventPosition()[1] << std::endl;


    //    vtkPointPicker *picker = (vtkPointPicker *)this->Interactor->GetPicker();
    vtkNew<vtkPointPicker> picker;
    
    picker->Pick(this->Interactor->GetEventPosition()[0],
                 this->Interactor->GetEventPosition()[1],
                 0, // always zero.
                 this->Interactor->GetRenderWindow()
                 ->GetRenderers()->GetFirstRenderer());

    std::cout << "PointId: " << picker->GetPointId() << std::endl;
    
    double *picked = picker->GetPickPosition();
    
    std::cout << "Picked value: " << picked[0] << " " << picked[1] << " "
              << picked[2] << std::endl;
    // Forward events
    vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
  }
};

vtkStandardNewMacro(MouseInteractorStyle);

class RendererTest : QVtkRenderer
{
  
public:

  RendererTest(char *filename) {

    displayPropertiesObj_.verticalExagg = 1.;    
    displayPropertiesObj_.showAxes = true;
    displayProperties_ = &displayPropertiesObj_;
    
    setGridFilename(filename);

    gridReader_ = vtkSmartPointer<GmtGridReader>::New();
    gridReader_->SetFileName(gridFilename_);
    gridReader_->Update();

    if (gridReader_->GetErrorCode()) {
      std::cerr << "Error during gridReader Update(): " <<
      gridReader_->GetErrorCode() << std::endl;
    
      return;
    }
    
    // Create pipeline objects and assemble them
    initializePipeline(filename);

    /* ***
    vtkSmartPointer<vtkCallbackCommand> makeCurrentCallbackCmd = 
      vtkSmartPointer<vtkCallbackCommand>::New();
    
    makeCurrentCallbackCmd->SetCallback ( &RendererTest::makeCurrentCallback );
    *** */
    
    vtkSmartPointer<vtkCallbackCommand> isCurrentCallbackCmd = 
      vtkSmartPointer<vtkCallbackCommand>::New();
    
    isCurrentCallbackCmd->SetCallback ( isCurrentCallback );    
  
    // Invoke callback when renderWindow_ is made current
    renderWindow_->AddObserver(vtkCommand::WindowMakeCurrentEvent,
                               this, &RendererTest::makeCurrentCallback);

    // Invoke callback when renderWindow_ is made current
    renderWindow_->AddObserver(vtkCommand::WindowIsCurrentEvent,
                               isCurrentCallbackCmd);    
  }
  

  void renderScene() {
    
    // Assemble pipeline -
    // pipeline already assembled by initializePipeline()
    //    assemblePipeline();
    //    initializeOpenGLState();
    // renderWindow_->Start();
    windowInteractor_->Start();
    windowInteractor_->EnableRenderOn();
    renderWindow_->Render();
  }


  int renderScene2(char *gridFilename, bool drawAxes) {

    windowInteractor_->EnableRenderOn();
    
    std::cout << "VTK Version: " << vtkVersion::GetVTKVersion() << std::endl;

    printf("in renderScene2() now\n");
    ColorMapScheme colorMapScheme = (ColorMapScheme )0;

    // Read the grid file
    std::string filePath = gridFilename;
   
   
    gridReader_->SetFileName ( filePath.c_str() );
    std::cout << "*** gridReader_->Update()" << std::endl;
    gridReader_->Update();
    if (gridReader_->GetErrorCode()) {
      std::cout << "Error during gridReader_->Update(): " \
                << gridReader_->GetErrorCode()
                << std::endl;
      return EXIT_FAILURE;
    }

    double zMin, zMax;
    gridReader_->zBounds(&zMin, &zMax);

    vtkAlgorithmOutput *port = nullptr;
   
    std::cout << "*** create elevationFilter" << std::endl;
    // Color data points based on z-value

    std::cout << "*** elevColorizer_->SetInputConnection\n";
    elevColorizer_->SetInputConnection(gridReader_->GetOutputPort());

    elevColorizer_->SetLowPoint(0, 0, zMin);
    elevColorizer_->SetHighPoint(0, 0, zMax);
    std::cout << "zMin: " << zMin << ", zMax: " << zMax << std::endl;

    port = elevColorizer_->GetOutputPort();

    // Visualize the data...

    std::cout << "*** surfaceMapper_->SetInputConnection()" << std::endl;
    surfaceMapper_->SetInputConnection(port);
    std::cout << "done setting connection\n";
   
    // Assign surfaceMapper_ to surfaceActor_
    std::cout << "*** assign surfaceMapper_ to surfaceActor_" << std::endl;      
    surfaceActor_->SetMapper(surfaceMapper_);

    if (drawAxes) {
      setupAxes(axesActor_, renderer_, gridReader_);
    }
    else {
    }

    // Add surfaceActor_ to the renderer
    std::cout << "*** renderer_->AddActor()" << std::endl;                        
    renderer_->AddActor(surfaceActor_);

    // Set to QVtkRenderer members by default
    vtkSmartPointer<vtkRenderWindow> renderWindow = renderWindow_;
    vtkSmartPointer<vtkRenderWindowInteractor> windowInteractor = \
      windowInteractor_;    

 /* ****     
    // Create new renderWindow 
   std::cout << "*** override renderWindow member" << std::endl;            
   renderWindow = vtkSmartPointer<vtkRenderWindow>::New();

   // Create new renderWindowInteractor
   std::cout << "*** overridee renderWindowInteractor member" << std::endl;      
   windowInteractor =
     vtkSmartPointer<vtkRenderWindowInteractor>::New();   

     *** */

    
   // Add renderer to the renderWindow
   std::cout << "*** add renderer to renderWindow" << std::endl;               
   renderWindow->AddRenderer(renderer_);

   vtkNew<MouseInteractorStyle> style;

   windowInteractor->SetInteractorStyle(style);
     
   windowInteractor->SetRenderWindow(renderWindow);

   renderer_->SetBackground(1.0, 1.0, 1.0);   

   renderer_->ResetCamera();

   std::cout << "*** renderWindowInteractor->Start()" << std::endl;
   windowInteractor_->Start();

   
   std::cout << "*** renderWindow->Render()" << std::endl;                        
   renderWindow->Render();

    return EXIT_SUCCESS;
  }

  void makeCurrentCallback(vtkObject *, unsigned long eid, void *callData) {

    std::cout << "makeCurrentCallback()!" << std::endl;

    renderWindow_->SetIsCurrent(true);
  }


  void setupAxes(vtkSmartPointer<vtkCubeAxesActor> axesActor,
                 vtkSmartPointer<vtkRenderer> renderer,
                 vtkSmartPointer<mb_system::GmtGridReader> reader) {

    // Colors for axes
    vtkSmartPointer<vtkNamedColors> colors = 
      vtkSmartPointer<vtkNamedColors>::New();

    vtkColor3d axisColor = colors->GetColor3d("Black");
    //  vtkColor3d labelColor = colors->GetColor3d("Red");

    // Axes actor
    axesActor = vtkSmartPointer<vtkCubeAxesActor>::New();
    axesActor->SetUseTextActor3D(0);


    axesActor->SetBounds(reader->GetOutput()->GetBounds());
    axesActor->SetCamera(renderer->GetActiveCamera());
    axesActor->GetTitleTextProperty(0)->SetColor(axisColor.GetData());
    axesActor->GetTitleTextProperty(0)->SetFontSize(48);
    axesActor->GetLabelTextProperty(0)->SetColor(axisColor.GetData());

    axesActor->GetTitleTextProperty(1)->SetColor(axisColor.GetData());
    axesActor->GetLabelTextProperty(1)->SetColor(axisColor.GetData());

    axesActor->GetTitleTextProperty(2)->SetColor(axisColor.GetData());
    axesActor->GetLabelTextProperty(2)->SetColor(axisColor.GetData());

    axesActor->GetXAxesLinesProperty()->SetColor(axisColor.GetData());
    axesActor->GetYAxesLinesProperty()->SetColor(axisColor.GetData());
    axesActor->GetZAxesLinesProperty()->SetColor(axisColor.GetData());
  
    axesActor->DrawXGridlinesOn();
    axesActor->DrawYGridlinesOn();
    axesActor->DrawZGridlinesOn();
  
    axesActor->SetXTitle("Easting");
    axesActor->SetYTitle("Northing");
    axesActor->SetZTitle("Depth");

#if VTK_MAJOR_VERSION == 6
    axesActor->SetGridLineLocation(VTK_GRID_LINES_FURTHEST);
#endif
#if VTK_MAJOR_VERSION > 6
    axesActor->SetGridLineLocation(
                                   axesActor->VTK_GRID_LINES_FURTHEST);
#endif
  
    axesActor->XAxisMinorTickVisibilityOff();
    axesActor->YAxisMinorTickVisibilityOff();
    axesActor->ZAxisMinorTickVisibilityOff();

    //  axesActor->SetFlyModeToStaticEdges();
  
    renderer->AddActor(axesActor);    
  }


  /// Hold display properties
  DisplayProperties displayPropertiesObj_;

};

/* ***
/// Make a callback function
class WindowCurrentCallback : public vtkCallbackCommand {
public:
  static WindowCurrentCallback *New() {
    return new WindowCurrentCallback;
  }
};
*** */
  

int main(int argc, char **argv) {

  if (argc != 2) {
    std::cerr << "usage: " << argv[0] << " gridFile" << std::endl;
    return -1;
  }

  char *filename = argv[1];

  RendererTest *renderer = new RendererTest(filename);

  std::cout << "renderScene2()" << std::endl;
  renderer->renderScene2(filename, true);
  
  std::cout << "All done" << std::endl;
  
  return 0;
}


