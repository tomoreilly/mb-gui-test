// These first three lines address
// issue described at
// https://stackoverflow.com/questions/18642155/no-override-found-for-vtkpolydatamapper
#include "vtkAutoInit.h" 
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

// This example reads and displays contents of a GMT grid file 
#include <vtkVersion.h>
#include <vtkSmartPointer.h>
#include <vtkCubeAxesActor.h>
#include <vtkCamera.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkActor2D.h>
#include <vtkProperty.h>
#include <vtkStringArray.h>
#include <vtkPolyDataMapper.h>
#include <vtkImageMapper.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkElevationFilter.h>
#include <vtkLookupTable.h>
#include <vtkColorSeries.h>
#include <vtkNamedColors.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <vtkDataObject.h>
#include <vtkGradientFilter.h>
#include <vtkInteractorStyleTerrain.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkTextProperty.h>
#include <vtkAbstractPicker.h>
#include <vtkPointPicker.h>
#include <vtkRendererCollection.h>
#include "Utilities.h"

#include "GmtGridReader.h"

using namespace mb_system;

/// Create a color lookup table
void makeLUT(int const& colorScheme, vtkLookupTable* lut);
void setupAxes(vtkSmartPointer<vtkCubeAxesActor> axesActor,
               vtkSmartPointer<vtkRenderer> renderer,
               vtkSmartPointer<mb_system::GmtGridReader> reader);


namespace {

// Define interaction style
class MouseInteractorStyle : public vtkInteractorStyleTrackballCamera
{
public:
  static MouseInteractorStyle* New();
  vtkTypeMacro(MouseInteractorStyle, vtkInteractorStyleTrackballCamera);

  virtual void OnLeftButtonDown() override
  {
    std::cout << "Picking pixel: " << this->Interactor->GetEventPosition()[0]
              << " " << this->Interactor->GetEventPosition()[1] << std::endl;


    //    vtkPointPicker *picker = (vtkPointPicker *)this->Interactor->GetPicker();
    vtkNew<vtkPointPicker> picker;
    
    picker->Pick(this->Interactor->GetEventPosition()[0],
                 this->Interactor->GetEventPosition()[1],
                 0, // always zero.
                 this->Interactor->GetRenderWindow()
                 ->GetRenderers()->GetFirstRenderer());

    std::cout << "PointId: " << picker->GetPointId() << std::endl;
    
    double *picked = picker->GetPickPosition();
    
    std::cout << "Picked value: " << picked[0] << " " << picked[1] << " "
              << picked[2] << std::endl;
    // Forward events
    vtkInteractorStyleTrackballCamera::OnLeftButtonDown();
  }
};
vtkStandardNewMacro(MouseInteractorStyle);

} // namespace


  // needed to easily convert int to std::string
int main(int argc, char* argv[])
{
  std::cout << "VTK Version: " << vtkVersion::GetVTKVersion() << std::endl;

  printf("in main() now\n");
  bool drawAxes = true;
  bool useLUT = false;
  ColorMapScheme colorMapScheme = (ColorMapScheme )0;

  bool showGradient = false;
  bool error = false;
  // Verify input arguments
   if ( argc < 2 )
   {
     error = true;
   }

   // Look for options preceding file name, which is always last arg
   for (int i = 1; i < argc-1; i++) {
     if (!strcmp(argv[i], "-grad")) {
       std::cout << "showGradient\n";
       showGradient = true;
     }
     else if (!strcmp(argv[i], "-lut") && i < argc-2) {
       useLUT = true;
       colorMapScheme = (ColorMapScheme )atoi(argv[++i]);
     }
     else {
       std::cerr << argv[i] << ": unknown option" << std::endl;
       error = true;
     }
     
   }

   if (error) {
      std::cerr << "Usage: " << argv[0]
      << " [-grad][-lut colorscheme] GMT-gridFile"  << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "showGradient: " << showGradient << std::endl;
   
   // Read the grid file
   std::string filePath = argv[argc-1];
   if (filePath[0] == '-') {
     std::cerr << filePath << ": invalid grid file name" << std::endl;
     return EXIT_FAILURE;
   }
   
   vtkSmartPointer<mb_system::GmtGridReader> reader =
     vtkSmartPointer<mb_system::GmtGridReader>::New();
   
   reader->SetFileName ( filePath.c_str() );
   std::cout << "*** reader->Update()" << std::endl;
   reader->Update();
   if (reader->GetErrorCode()) {
     std::cout << "Error during reader->Update(): " << reader->GetErrorCode()
               << std::endl;
     return EXIT_FAILURE;
   }

   double zMin, zMax;
   reader->zBounds(&zMin, &zMax);

   vtkAlgorithmOutput *port = nullptr;
   
   std::cout << "*** create elevationFilter" << std::endl;
   // Color data points based on z-value
   vtkSmartPointer<vtkElevationFilter> elevationFilter =
     vtkSmartPointer<vtkElevationFilter>::New();

   std::cout << "*** elevationFilter->SetInputConnection\n";
   elevationFilter->SetInputConnection(reader->GetOutputPort());

   elevationFilter->SetLowPoint(0, 0, zMin);
   elevationFilter->SetHighPoint(0, 0, zMax);
   std::cout << "zMin: " << zMin << ", zMax: " << zMax << std::endl;

   std::cout << "showGradient: " << showGradient << std::endl;
   
   port = elevationFilter->GetOutputPort();
   if (useLUT) {
     if (!showGradient) {
       elevationFilter->SetScalarRange(zMin, zMax);
     }
   }

   vtkSmartPointer<vtkGradientFilter> gradientFilter =
     vtkSmartPointer<vtkGradientFilter>::New();

   if (showGradient) {

     gradientFilter->SetInputConnection(port);
     port = gradientFilter->GetOutputPort();
   }
   
   // Visualize the data...

   // Create renderer
   std::cout << "*** create renderer" << std::endl;         
   vtkSmartPointer<vtkRenderer> renderer =
      vtkSmartPointer<vtkRenderer>::New();

   // Create gridMapper
   std::cout << "*** create gridMapper" << std::endl;
   vtkSmartPointer<vtkPolyDataMapper> gridMapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();

   std::cout << "*** gridMapper->SetInputConnection()" << std::endl;
   gridMapper->SetInputConnection(port);
   std::cout << "done setting connection\n";
   
   if (useLUT) {
     if (showGradient)  {
       zMin = gradientFilter->GetOutput()->GetScalarRange()[0];
       zMin = gradientFilter->GetOutput()->GetScalarRange()[1];       
       std::cout << " zMIn: " << zMin << ", zMax: " << zMax << std::endl;
     }
     
     vtkSmartPointer<vtkLookupTable> lut =
       vtkSmartPointer<vtkLookupTable>::New();

     std::cout << "colorMapScheme: " << colorMapScheme << std::endl;
     makeLookupTable(colorMapScheme, lut);

     std::cout << "SetScalarRange " << zMin << "  " << zMax << std::endl;
     gridMapper->SetScalarRange(zMin, zMax);
     gridMapper->ScalarVisibilityOn();
     gridMapper->SetLookupTable(lut);
   }
   
   // Create actor
   std::cout << "*** create actor" << std::endl;   
   vtkSmartPointer<vtkActor> actor =
      vtkSmartPointer<vtkActor>::New();

   // Assign gridMapper to actor
   std::cout << "*** assign gridMapper to actor" << std::endl;      
   actor->SetMapper(gridMapper);

   if (drawAxes) {
     vtkSmartPointer<vtkCubeAxesActor> cubeAxesActor =
       vtkSmartPointer<vtkCubeAxesActor>::New();

     setupAxes(cubeAxesActor, renderer, reader);
   }
   else {
   }

   // Add actor to the renderer
   std::cout << "*** rendererr->AddActor()" << std::endl;                        
   renderer->AddActor(actor);
   
   // Create renderWindow
   std::cout << "*** create renderWindow" << std::endl;            
   vtkSmartPointer<vtkRenderWindow> renderWindow =
     vtkSmartPointer<vtkRenderWindow>::New();

   // Add renderer to the renderWindow
   std::cout << "*** add renderer to renderWindow" << std::endl;               
   renderWindow->AddRenderer(renderer);

   // Create renderWindowInteractor
   std::cout << "*** create renderWindowInteractor" << std::endl;                  
   vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
     vtkSmartPointer<vtkRenderWindowInteractor>::New();

   // Set interactor style
   //   vtkSmartPointer<vtkInteractorStyleTerrain> style =
   // vtkSmartPointer<vtkInteractorStyleTerrain>::New();

   // vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
   // vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();

   vtkNew<MouseInteractorStyle> style;
   renderWindowInteractor->SetInteractorStyle(style);

   renderWindowInteractor->SetInteractorStyle(style);
     
   renderWindowInteractor->SetRenderWindow(renderWindow);

   renderer->SetBackground(1.0, 1.0, 1.0);   

   renderer->ResetCamera();

   std::cout << "*** renderWindowInteractor->Start()" << std::endl;
   renderWindowInteractor->Start();

   std::cout << "*** renderWindow->Render()" << std::endl;                        
   renderWindow->Render();

   return EXIT_SUCCESS;
}


/* ***
void makeLUT(int const& colorScheme, vtkLookupTable* lut)
{
  vtkSmartPointer<vtkNamedColors> colors =
    vtkSmartPointer<vtkNamedColors>::New();
  
  // Select a color scheme.
  switch (colorScheme)
    {
    case 0:
    default:
      {
	// Make the lookup using a Brewer palette.
	vtkSmartPointer<vtkColorSeries> colorSeries =
	  vtkSmartPointer<vtkColorSeries>::New();
	colorSeries->SetNumberOfColors(8);
	int colorSeriesEnum = colorSeries->BREWER_DIVERGING_BROWN_BLUE_GREEN_8;
	colorSeries->SetColorScheme(colorSeriesEnum);
	colorSeries->BuildLookupTable(lut, colorSeries->ORDINAL);
	lut->SetNanColor(1, 0, 0, 1);
	break;
      }
    case 1:
      {
	// A lookup table of 256 colours ranging from
	//  deep blue(water) to yellow - white(mountain top)
	//  is used to color map this figure.
	lut->SetHueRange(0.7, 0);
	lut->SetSaturationRange(1.0, 0);
	lut->SetValueRange(0.5, 1.0);
	break;
      }
    case 2:
      {
      // Make the lookup table with a preset number of colours.
      vtkSmartPointer<vtkColorSeries> colorSeries =
        vtkSmartPointer<vtkColorSeries>::New();
      colorSeries->SetNumberOfColors(8);
      colorSeries->SetColorSchemeName("Hawaii");
      colorSeries->SetColor(0, colors->GetColor3ub("turquoise_blue"));
      colorSeries->SetColor(1, colors->GetColor3ub("sea_green_medium"));
      colorSeries->SetColor(2, colors->GetColor3ub("sap_green"));
      colorSeries->SetColor(3, colors->GetColor3ub("green_dark"));
      colorSeries->SetColor(4, colors->GetColor3ub("tan"));
      colorSeries->SetColor(5, colors->GetColor3ub("beige"));
      colorSeries->SetColor(6, colors->GetColor3ub("light_beige"));
      colorSeries->SetColor(7, colors->GetColor3ub("bisque"));
      colorSeries->BuildLookupTable(lut, colorSeries->ORDINAL);
      lut->SetNanColor(1, 0, 0, 1);
      break;
      }
    case 3: {
      // A lookup table of 256 colours ranging from
      //  deep blue(water) to yellow - white(mountain top)
      //  is used to color map this figure.
      lut->SetHueRange(0.7, 0.06);
      lut->SetSaturationRange(1.0, 0.78);
      lut->SetValueRange(0.5, 0.74);

      break;
    }

      
    };
}

*** */


void setupAxes(vtkSmartPointer<vtkCubeAxesActor> axesActor,
               vtkSmartPointer<vtkRenderer> renderer,
               vtkSmartPointer<mb_system::GmtGridReader> reader) {

  // Colors for axes
  vtkSmartPointer<vtkNamedColors> colors = 
    vtkSmartPointer<vtkNamedColors>::New();

  vtkColor3d axisColor = colors->GetColor3d("Black");
  //  vtkColor3d labelColor = colors->GetColor3d("Red");

  // Axes actor
  axesActor = vtkSmartPointer<vtkCubeAxesActor>::New();
  axesActor->SetUseTextActor3D(0);


  axesActor->SetBounds(reader->GetOutput()->GetBounds());
  axesActor->SetCamera(renderer->GetActiveCamera());
  axesActor->GetTitleTextProperty(0)->SetColor(axisColor.GetData());
  axesActor->GetTitleTextProperty(0)->SetFontSize(48);
  axesActor->GetLabelTextProperty(0)->SetColor(axisColor.GetData());

  axesActor->GetTitleTextProperty(1)->SetColor(axisColor.GetData());
  axesActor->GetLabelTextProperty(1)->SetColor(axisColor.GetData());

  axesActor->GetTitleTextProperty(2)->SetColor(axisColor.GetData());
  axesActor->GetLabelTextProperty(2)->SetColor(axisColor.GetData());

  axesActor->GetXAxesLinesProperty()->SetColor(axisColor.GetData());
  axesActor->GetYAxesLinesProperty()->SetColor(axisColor.GetData());
  axesActor->GetZAxesLinesProperty()->SetColor(axisColor.GetData());
  
  axesActor->DrawXGridlinesOn();
  axesActor->DrawYGridlinesOn();
  axesActor->DrawZGridlinesOn();
  
  axesActor->SetXTitle("Easting");
  axesActor->SetYTitle("Northing");
  axesActor->SetZTitle("Depth");

#if VTK_MAJOR_VERSION == 6
  axesActor->SetGridLineLocation(VTK_GRID_LINES_FURTHEST);
#endif
#if VTK_MAJOR_VERSION > 6
  axesActor->SetGridLineLocation(
				  axesActor->VTK_GRID_LINES_FURTHEST);
#endif
  
  axesActor->XAxisMinorTickVisibilityOff();
  axesActor->YAxisMinorTickVisibilityOff();
  axesActor->ZAxisMinorTickVisibilityOff();

  //  axesActor->SetFlyModeToStaticEdges();
  
  renderer->AddActor(axesActor);    
}


