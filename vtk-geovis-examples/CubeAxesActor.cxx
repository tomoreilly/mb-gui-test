// These first three lines address
// issue described at
// https://stackoverflow.com/questions/18642155/no-override-found-for-vtkpolydatamapper
#include "vtkAutoInit.h" 
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

#include <vtkPolyDataMapper.h>
#include <vtkCubeAxesActor.h>
#include <vtkTextProperty.h>
#include <vtkActorCollection.h>
#include <vtkCamera.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkPolyData.h>
#include <vtkSmartPointer.h>
#include <vtkSuperquadricSource.h>
#include <vtkActor.h>
#include <vtkTransform.h>
#include <vtkTransformFilter.h>

int main(int argc, char *argv[])
{
  float zScale = 1.;
  
  if (argc > 1) {
    zScale = atof(argv[1]);
  }  

  // Create a superquadric
  vtkSmartPointer<vtkSuperquadricSource> superquadricSource = 
    vtkSmartPointer<vtkSuperquadricSource>::New();
  superquadricSource->SetPhiRoundness(3.1);
  superquadricSource->SetThetaRoundness(1.0);
  superquadricSource->Update(); // needed to GetBounds later

  vtkNew<vtkTransform> transform;
  transform->Scale(1., 1., zScale);
  vtkNew<vtkTransformFilter> filter;
  filter->SetTransform(transform);
  filter->SetInputConnection(superquadricSource->GetOutputPort());
  
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();

  vtkSmartPointer<vtkPolyDataMapper> mapper =
    vtkSmartPointer<vtkPolyDataMapper>::New();
  mapper->SetInputConnection(filter->GetOutputPort());

  vtkSmartPointer<vtkActor> superquadricActor =
    vtkSmartPointer<vtkActor>::New();
  superquadricActor->SetMapper(mapper);

  std::cout << "create cubeAxesActor" << std::endl;
  
  vtkSmartPointer<vtkCubeAxesActor> cubeAxesActor =
    vtkSmartPointer<vtkCubeAxesActor>::New();

  double *bounds = mapper->GetBounds();
  //  cubeAxesActor.SetBounds(superquadricSource.GetOutput().GetBounds())

  std::cout << "bounds[0] " << bounds[0] << " bounds[1] " << bounds[1] <<
    std::endl;
  std::cout << "bounds[2] " << bounds[2] << " bounds[3] " << bounds[3] <<
    std::endl;
  std::cout << "bounds[4] " << bounds[4] << " bounds[5] " << bounds[5] <<
    std::endl;    

  //  bounds[4] = -0.2;
  //  bounds[5] = 0.2;
  cubeAxesActor->SetBounds(bounds);

  cubeAxesActor->SetCamera(renderer->GetActiveCamera());
  cubeAxesActor->GetTitleTextProperty(0)->SetColor(1.0, 0.0, 0.0);
  cubeAxesActor->GetLabelTextProperty(0)->SetColor(1.0, 0.0, 0.0);

  cubeAxesActor->GetTitleTextProperty(1)->SetColor(0.0, 1.0, 0.0);
  cubeAxesActor->GetLabelTextProperty(1)->SetColor(0.0, 1.0, 0.0);

  cubeAxesActor->GetTitleTextProperty(2)->SetColor(0.0, 0.0, 1.0);
  cubeAxesActor->GetLabelTextProperty(2)->SetColor(0.0, 0.0, 1.0);

  cubeAxesActor->DrawXGridlinesOn();
  cubeAxesActor->DrawYGridlinesOn();
  cubeAxesActor->DrawZGridlinesOn();
#if VTK_MAJOR_VERSION == 6
  cubeAxesActor->SetGridLineLocation(VTK_GRID_LINES_FURTHEST);
#endif
#if VTK_MAJOR_VERSION > 6
  cubeAxesActor->SetGridLineLocation(
    cubeAxesActor->VTK_GRID_LINES_FURTHEST);
#endif
  
  cubeAxesActor->XAxisMinorTickVisibilityOff();
  cubeAxesActor->YAxisMinorTickVisibilityOff();
  cubeAxesActor->ZAxisMinorTickVisibilityOff();

  renderer->AddActor(cubeAxesActor);
  renderer->AddActor(superquadricActor);
  renderer->GetActiveCamera()->Azimuth(30);
  renderer->GetActiveCamera()->Elevation(30);

  renderer->ResetCamera();

  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);

  vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  renderWindowInteractor->SetRenderWindow(renderWindow);

  renderWindow->Render();
  renderWindowInteractor->Start();

  return EXIT_SUCCESS;
}
