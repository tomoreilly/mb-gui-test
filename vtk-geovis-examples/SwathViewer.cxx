// These first three lines address
// issue described at
// https://stackoverflow.com/questions/18642155/no-override-found-for-vtkpolydatamapper
#include "vtkAutoInit.h" 
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

// This example reads and displays contents of a sonar swath file 
#include <vtkSmartPointer.h>
#include <vtkCubeAxesActor.h>
#include <vtkCamera.h>
#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkActor.h>
#include <vtkActorCollection.h>
#include <vtkActor2D.h>
#include <vtkProperty.h>
#include <vtkStringArray.h>
#include <vtkPolyDataMapper.h>
#include <vtkImageMapper.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkElevationFilter.h>
#include <vtkLookupTable.h>
#include <vtkColorSeries.h>
#include <vtkNamedColors.h>
#include <vtkUnsignedCharArray.h>
#include <vtkPointData.h>
#include <vtkDataObject.h>
#include <vtkGradientFilter.h>
#include <vtkInteractorStyleTerrain.h>
#include <vtkInteractorStyleTrackballCamera.h>

#include "SwathReader.h"

/// Create a color lookup table
void makeLUT(int const& colorScheme, vtkLookupTable* lut);


  // needed to easily convert int to std::string
int main(int argc, char* argv[])
{
  bool drawAxes = false;
  bool useLUT = false;
  int colorScheme = 0;

  bool showGradient = false;
  bool error = false;
  // Verify input arguments
   if ( argc < 2 )
   {
     error = true;
   }

   // Look for options preceding file name, which is always last arg
   for (int i = 1; i < argc-1; i++) {
     if (!strcmp(argv[i], "-grad")) {
       std::cout << "showGradient\n";
       showGradient = true;
     }
     else if (!strcmp(argv[i], "-lut") && i < argc-2) {
       useLUT = true;
       colorScheme = atoi(argv[++i]);
     }
     else {
       std::cerr << argv[i] << ": unknown option" << std::endl;
       error = true;
     }
     
   }

   if (error) {
      std::cerr << "Usage: " << argv[0]
      << " [-grad][-lut colorscheme] swathFile"  << std::endl;
      return EXIT_FAILURE;
   }

   std::cout << "showGradient: " << showGradient << std::endl;
   
   // Read the grid file
   std::string filePath = argv[argc-1];
   if (filePath[0] == '-') {
     std::cerr << filePath << ": invalid grid file name" << std::endl;
     return EXIT_FAILURE;
   }
   
   vtkSmartPointer<mb_system::SwathReader> reader =
     vtkSmartPointer<mb_system::SwathReader>::New();
   
   reader->SetFileName ( filePath.c_str() );
   std::cout << "*** reader->Update()" << std::endl;
   reader->Update();
   if (reader->GetErrorCode()) {
     std::cout << "Error during reader->Update(): " << reader->GetErrorCode()
               << std::endl;
     return EXIT_FAILURE;
   }

   double zMin, zMax;
   reader->zBounds(&zMin, &zMax);

   vtkAlgorithmOutput *port = nullptr;
   
   std::cout << "*** create elevationFilter" << std::endl;
   // Color data points based on z-value
   vtkSmartPointer<vtkElevationFilter> elevationFilter =
     vtkSmartPointer<vtkElevationFilter>::New();

   std::cout << "*** elevationFilter->SetInputConnection\n";
   elevationFilter->SetInputConnection(reader->GetOutputPort());

   elevationFilter->SetLowPoint(0, 0, zMin);
   elevationFilter->SetHighPoint(0, 0, zMax);
   port = elevationFilter->GetOutputPort();
   if (useLUT) {
     if (!showGradient) {
       elevationFilter->SetScalarRange(zMin, zMax);
     }
   }

   vtkSmartPointer<vtkGradientFilter> gradientFilter =
     vtkSmartPointer<vtkGradientFilter>::New();

   if (showGradient) {

     gradientFilter->SetInputConnection(port);
     port = gradientFilter->GetOutputPort();
   }
   
   // Visualize the data...

   // Create renderer
   std::cout << "*** create renderer" << std::endl;         
   vtkSmartPointer<vtkRenderer> renderer =
      vtkSmartPointer<vtkRenderer>::New();

   // Create mapper
   std::cout << "*** create mapper" << std::endl;
   vtkSmartPointer<vtkPolyDataMapper> mapper =
      vtkSmartPointer<vtkPolyDataMapper>::New();

   std::cout << "*** mapper->SetInputConnection()" << std::endl;
   mapper->SetInputConnection(port);
   std::cout << "done setting connection\n";
   
   if (useLUT) {
     if (showGradient)  {
       zMin = gradientFilter->GetOutput()->GetScalarRange()[0];
       zMin = gradientFilter->GetOutput()->GetScalarRange()[1];       
       std::cout << " zMIn: " << zMin << ", zMax: " << zMax << std::endl;
     }
     
     vtkSmartPointer<vtkLookupTable> lut = vtkSmartPointer<vtkLookupTable>::New();
     makeLUT(colorScheme, lut);
     
     mapper->SetScalarRange(zMin, zMax);
     mapper->ScalarVisibilityOn();
     mapper->SetLookupTable(lut);
   }
   
   // Create actor
   std::cout << "*** create actor" << std::endl;   
   vtkSmartPointer<vtkActor> actor =
      vtkSmartPointer<vtkActor>::New();

   // Assign mapper to actor
   std::cout << "*** assign mapper to actor" << std::endl;      
   actor->SetMapper(mapper);

   if (drawAxes) {
     vtkSmartPointer<vtkCubeAxesActor> cubeAxesActor =
       vtkSmartPointer<vtkCubeAxesActor>::New();
     cubeAxesActor->SetUseTextActor3D(1);
     cubeAxesActor->SetBounds(reader->GetOutput()->GetBounds());
     //   cubeAxesActor->SetCamera(camera);
     cubeAxesActor->SetCamera(renderer->GetActiveCamera());   

     cubeAxesActor->XAxisMinorTickVisibilityOff();
     cubeAxesActor->YAxisMinorTickVisibilityOff();
     cubeAxesActor->ZAxisMinorTickVisibilityOff();

     cubeAxesActor->SetFlyModeToStaticEdges();

     std::cout << "*** rendererr->AddActor(cubeAxesActor)" << std::endl;                        
     renderer->AddActor(cubeAxesActor);
   }
   else {
   }
   
   // Add actor to the renderer
   std::cout << "*** rendererr->AddActor()" << std::endl;                        
   renderer->AddActor(actor);
   
   // Create renderWindow
   std::cout << "*** create renderWindow" << std::endl;            
   vtkSmartPointer<vtkRenderWindow> renderWindow =
     vtkSmartPointer<vtkRenderWindow>::New();

   // Add renderer to the renderWindow
   std::cout << "*** add renderer to renderWindow" << std::endl;               
   renderWindow->AddRenderer(renderer);

   // Create renderWindowInteractor
   std::cout << "*** create renderWindowInteractor" << std::endl;                  
   vtkSmartPointer<vtkRenderWindowInteractor> renderWindowInteractor =
     vtkSmartPointer<vtkRenderWindowInteractor>::New();

   // Set interactor style
   //   vtkSmartPointer<vtkInteractorStyleTerrain> style =
   // vtkSmartPointer<vtkInteractorStyleTerrain>::New();

   vtkSmartPointer<vtkInteractorStyleTrackballCamera> style =
     vtkSmartPointer<vtkInteractorStyleTrackballCamera>::New();
   
   renderWindowInteractor->SetInteractorStyle(style);
     
   std::cout << "*** renderWindowInteractor->SetRenderWindow()" << std::endl;                     renderWindowInteractor->SetRenderWindow(renderWindow);

   renderer->SetBackground(.2, .3, .4);   

   renderer->ResetCamera();

   std::cout << "*** renderWindowInteractor->Start()" << std::endl;
   renderWindowInteractor->Start();

   std::cout << "*** renderWindow->Render()" << std::endl;                        
   renderWindow->Render();

   return EXIT_SUCCESS;
}



void makeLUT(int const& colorScheme, vtkLookupTable* lut)
{
  vtkSmartPointer<vtkNamedColors> colors =
    vtkSmartPointer<vtkNamedColors>::New();
  
  // Select a color scheme.
  switch (colorScheme)
    {
    case 0:
    default:
      {
	// Make the lookup using a Brewer palette.
	vtkSmartPointer<vtkColorSeries> colorSeries =
	  vtkSmartPointer<vtkColorSeries>::New();
	colorSeries->SetNumberOfColors(8);
	int colorSeriesEnum = colorSeries->BREWER_DIVERGING_BROWN_BLUE_GREEN_8;
	colorSeries->SetColorScheme(colorSeriesEnum);
	colorSeries->BuildLookupTable(lut, colorSeries->ORDINAL);
	lut->SetNanColor(1, 0, 0, 1);
	break;
      }
    case 1:
      {
	// A lookup table of 256 colours ranging from
	//  deep blue(water) to yellow - white(mountain top)
	//  is used to color map this figure.
	lut->SetHueRange(0.7, 0);
	lut->SetSaturationRange(1.0, 0);
	lut->SetValueRange(0.5, 1.0);
	break;
      }
    case 2:
      {
      // Make the lookup table with a preset number of colours.
      vtkSmartPointer<vtkColorSeries> colorSeries =
        vtkSmartPointer<vtkColorSeries>::New();
      colorSeries->SetNumberOfColors(8);
      colorSeries->SetColorSchemeName("Hawaii");
      colorSeries->SetColor(0, colors->GetColor3ub("turquoise_blue"));
      colorSeries->SetColor(1, colors->GetColor3ub("sea_green_medium"));
      colorSeries->SetColor(2, colors->GetColor3ub("sap_green"));
      colorSeries->SetColor(3, colors->GetColor3ub("green_dark"));
      colorSeries->SetColor(4, colors->GetColor3ub("tan"));
      colorSeries->SetColor(5, colors->GetColor3ub("beige"));
      colorSeries->SetColor(6, colors->GetColor3ub("light_beige"));
      colorSeries->SetColor(7, colors->GetColor3ub("bisque"));
      colorSeries->BuildLookupTable(lut, colorSeries->ORDINAL);
      lut->SetNanColor(1, 0, 0, 1);
      break;
      }
    case 3: {
      // A lookup table of 256 colours ranging from
      //  deep blue(water) to yellow - white(mountain top)
      //  is used to color map this figure.
      lut->SetHueRange(0.7, 0.06);
      lut->SetSaturationRange(1.0, 0.78);
      lut->SetValueRange(0.5, 0.74);

      break;
    }

      
    };
}
