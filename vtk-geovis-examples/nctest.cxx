// These first three lines address
// issue described at
// https://stackoverflow.com/questions/18642155/no-override-found-for-vtkpolydatamapper
#include "vtkAutoInit.h" 
VTK_MODULE_INIT(vtkRenderingOpenGL2); // VTK was built with vtkRenderingOpenGL2
VTK_MODULE_INIT(vtkInteractionStyle);

#include <vtkRenderWindow.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkRenderer.h>
#include <vtkSmartPointer.h>
#include <vtkStructuredPointsReader.h>
#include <vtkPiecewiseFunction.h>
#include <vtkSmartVolumeMapper.h>
#include <vtkColorTransferFunction.h>
#include <vtkVolume.h>
#include <vtkVolumeProperty.h>
#include <vtkRectilinearGridReader.h>
#include <vtkSmartPointer.h>
#include <vtkNetCDFReader.h>
#include <vtkNamedColors.h>

/** Based on this example: https://discourse.vtk.org/t/volume-rendering-error/2690 */

int main(int argc, char **argv)
 {
   // Verify input arguments
   if ( argc != 2 )
   {
      std::cout << "Usage: " << argv[0]
      << " netCdfFile" << std::endl;
      return EXIT_FAILURE;
   }

     // Add named color library
     vtkSmartPointer<vtkNamedColors> colors = vtkSmartPointer<vtkNamedColors>::New();

     // Create renderer
     vtkSmartPointer<vtkRenderer> renderer = vtkSmartPointer<vtkRenderer>::New();

     // Create a new render window
     vtkSmartPointer<vtkRenderWindow> renWin = vtkSmartPointer<vtkRenderWindow>::New();
     renWin->AddRenderer(renderer);

     // Make the render window interacting
     vtkSmartPointer<vtkRenderWindowInteractor> iren = vtkSmartPointer<vtkRenderWindowInteractor>::New();
     iren->SetRenderWindow(renWin);

     vtkSmartPointer<vtkStructuredPointsReader> reader = vtkSmartPointer<vtkStructuredPointsReader>::New();
     
     std::string filenameNC = argv[1];

     vtkSmartPointer<vtkNetCDFReader> cdfReader = vtkSmartPointer<vtkNetCDFReader>::New();
     cdfReader->SetFileName(filenameNC.c_str());
     cdfReader->UpdateInformation();
     cdfReader->SetDimensions("z,y,x");
     cdfReader->SetVariableArrayStatus("ql_3D", 1);
  
     // Add a piece-wise function for color transfer functions. Piece-wise means 
     // adding control (interpolation) points.
     vtkSmartPointer<vtkPiecewiseFunction> opacityTransferFunction = vtkSmartPointer<vtkPiecewiseFunction>::New();
     opacityTransferFunction->AddPoint(0, 0.0);
     opacityTransferFunction->AddPoint(0.342, 1.0);

     // Piece-wise function cannot be used for colors because colors are vectors
     vtkSmartPointer<vtkColorTransferFunction> colorTransferFunction = vtkSmartPointer<vtkColorTransferFunction> ::New();
     colorTransferFunction->AddRGBPoint(0, 0.23, 0.29, 0.75);
     colorTransferFunction->AddRGBPoint(0.171, 0.860, 0.860, 0.860);
     colorTransferFunction->AddRGBPoint(0.342, 0.70, 0.010, 0.140);


     // Set volume rendering properties
     vtkSmartPointer<vtkVolumeProperty> volumeProperty = vtkSmartPointer<vtkVolumeProperty>::New();
     volumeProperty->SetColor(colorTransferFunction);
     volumeProperty->SetScalarOpacity(opacityTransferFunction);
     volumeProperty->ShadeOn();
     volumeProperty->SetInterpolationTypeToLinear();

     // Add a mapper to create graphic primitives from the data
     vtkSmartPointer<vtkSmartVolumeMapper> mapper = vtkSmartPointer<vtkSmartVolumeMapper>::New();
     mapper->SetBlendModeToComposite();
     mapper->SetInputConnection(cdfReader->GetOutputPort());

     // Create a new actor(the actual graphics object) and add the mapped data to 
     // it
     vtkSmartPointer<vtkVolume> volume = vtkSmartPointer<vtkVolume>::New();
     volume->SetMapper(mapper);
     volume->SetProperty(volumeProperty);

     // Add the volume actor to the renderer
     renderer->AddVolume(volume);

     // Set the background color

     renderer->SetBackground(colors->GetColor3d("Black").GetData());

     // Set the size of the render window
     renWin->SetSize(512, 512);

     // Render the data 
     renWin->Render();

     // Start the interactor
     iren->Start();

     return 0;
 }
